

#include "stdafx.h"

#include "XDMDNative.h"

#include "dsfutil.h"
#include "dshowutil.h"

#ifndef LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR
#define LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR    0x00000100
#endif

#ifndef LOAD_LIBRARY_SEARCH_DEFAULT_DIRS
#define LOAD_LIBRARY_SEARCH_DEFAULT_DIRS    0x00001000
#endif

HINSTANCE hVpmDLL = NULL;

HMODULE hModule;

typedef int(*Open_t)();
Open_t DmdDev_Open;

typedef bool(*Close_t)();
Close_t DmdDev_Close;

typedef void(*Set_16_Colors_Palette_t)(rgb24 *color);
Set_16_Colors_Palette_t DmdDev_Set_16_Colors_Palette;

typedef void(*Render_16_Shades_t)(UINT16 width, UINT16 height, UINT8 *currbuffer);
Render_16_Shades_t DmdDev_Render_16_Shades;

typedef void(*Render_RGB24_t)(UINT16 width, UINT16 height, rgb24 *currbuffer);
Render_RGB24_t DmdDev_Render_RGB24;

using namespace System::Runtime::InteropServices;

void XDMDNative::SetPINDMD2Color(array<RGB24> ^b)

{


	char *OutputPacketBufferc;
	OutputPacketBufferc = (char *)malloc(2052);
	memset(OutputPacketBufferc, 0, 2052);

	const UINT8 tmp[7 + 16 * 3] = {
		0x81, 0xC3, 0xE7, 0xFF, 0x04, 0x00, 0x01, 
		b[0].R, b[0].G, b[0].B, 
		b[1].R, b[1].G, b[1].B, 
		b[2].R, b[2].G, b[2].B, 
		b[3].R, b[3].G, b[3].B, 
		b[4].R, b[4].G, b[4].B, 
		b[5].R, b[5].G, b[5].B, 
		b[6].R, b[6].G, b[6].B, 
		b[7].R, b[7].G, b[7].B, 
		b[8].R, b[8].G, b[8].B, 
		b[9].R, b[9].G, b[9].B, 
		b[10].R, b[10].G, b[10].B, 
		b[11].R, b[11].G, b[11].B, 
		b[12].R, b[12].G, b[12].B, 
		b[13].R, b[13].G, b[13].B, 
		b[14].R, b[14].G, b[14].B, 
		b[15].R, b[15].G, b[15].B }; 

	memcpy(OutputPacketBufferc, tmp, sizeof(tmp));

	usb_bulk_write(device, EP_OUT, OutputPacketBufferc, 2052, 5000);
	Sleep(50);
	free(OutputPacketBufferc);

}


void XDMDNative::SetState(PLAYER_STATE state)
{
	m_State = state;
}

PLAYER_STATE XDMDNative::State()
{
	return m_State;
}
void XDMDNative::_Sleep(int ms)
{
	Sleep(ms);

}

bool XDMDNative::Play()
{
	
		HRESULT hr = S_OK;
		
	if (pC->m_pMediaControl != NULL)
	{
		if (m_State == PLAYER_STATE::PLAYER_STATE_Playing)
			return true;
	
	
	
	
	
	
	
 


	
	
	
	
	
	
	
	

		
		
		hr = pC->m_pMediaControl->Run();
	/*	long evCode;
    pC->m_pMediaEvent->WaitForCompletion(INFINITE, &evCode);*/


	if (SUCCEEDED(hr))
	{
		SetState(PLAYER_STATE::PLAYER_STATE_Playing);
	
		
		
					 /*OAFilterState state;
	pC->m_pMediaControl->GetState(20000, &state);*/

		
	
		}
	}
	
		return true;

}

bool XDMDNative::PlayWait()
{
		HRESULT hr = S_OK;

	if (pC->m_pMediaControl != NULL)
	{
		if (m_State == PLAYER_STATE::PLAYER_STATE_Playing)
			return true;
		
		hr = pC->m_pMediaControl->Run();
	/*	long evCode;
    pC->m_pMediaEvent->WaitForCompletion(INFINITE, &evCode);*/


	if (SUCCEEDED(hr))
		SetState(PLAYER_STATE::PLAYER_STATE_Playing);
	
		
		 OAFilterState state;
	pC->m_pMediaControl->GetState(20000, &state);
				
		}
		
		return true;

}

void  XDMDNative::MyDeleteMediaType(AM_MEDIA_TYPE *pmt)
{
	return;
    if (pmt != NULL)
    {
      
		
        CoTaskMemFree(pmt);
    }
}

STDMETHODIMP XDMDNative::GrabSample()
{
	if (pC->m_pSampleGrabber == NULL)
		return E_FAIL;

	HRESULT hr;

	if (W == 0)
	{
		cntgrabs =0;
		AM_MEDIA_TYPE mt;
		hr = pC->m_pSampleGrabber->GetConnectedMediaType(&mt);
		
	/*	VIDEOINFOHEADER *pVih;*/
		if ((mt.formattype == FORMAT_VideoInfo) && 
			(mt.cbFormat >= sizeof(VIDEOINFOHEADER)) &&
			(mt.pbFormat != NULL) ) 
		{
				
			pVih = (VIDEOINFOHEADER*)mt.pbFormat;
			W=pVih->bmiHeader.biWidth;
			H=pVih->bmiHeader.biHeight;
			
		
		
		}
		else 
		{
			
			if (mt.cbFormat != 0)
		{
			CoTaskMemFree((PVOID)mt.pbFormat);
			mt.cbFormat = 0;
			mt.pbFormat = NULL;
		}
		if (mt.pUnk != NULL)
		{
			
			mt.pUnk->Release();
			mt.pUnk = NULL;
		}

		
			return VFW_E_INVALIDMEDIATYPE; 
		}


		if (mt.cbFormat != 0)
		{
			CoTaskMemFree((PVOID)mt.pbFormat);
			mt.cbFormat = 0;
			mt.pbFormat = NULL;
		}
		if (mt.pUnk != NULL)
		{
			
			mt.pUnk->Release();
			mt.pUnk = NULL;
		}

		
		MyDeleteMediaType(&mt);
	}


	
	return S_OK;
}

bool XDMDNative::GrabSampleToDC(int HD,int width,int height)
{

	
	if (pC->m_pSampleGrabber == NULL)
		return false;


	HRESULT hr;

	AM_MEDIA_TYPE mt;
		hr = pC->m_pSampleGrabber->GetConnectedMediaType(&mt);
		

		if ((mt.formattype == FORMAT_VideoInfo) && 
			(mt.cbFormat >= sizeof(VIDEOINFOHEADER)) &&
			(mt.pbFormat != NULL) ) 
		{
				
			pVih = (VIDEOINFOHEADER*)mt.pbFormat;
		
		}
		else 
		{

			
			if (mt.cbFormat != 0)
		{
			CoTaskMemFree((PVOID)mt.pbFormat);
			mt.cbFormat = 0;
			mt.pbFormat = NULL;
		}
		if (mt.pUnk != NULL)
		{
			
			/*mt.pUnk->Release();
			mt.pUnk = NULL;*/
		}

		MyDeleteMediaType(&mt);
			return false; 
		}

		if (mt.cbFormat != 0)
		{
			CoTaskMemFree((PVOID)mt.pbFormat);
			mt.cbFormat = 0;
			mt.pbFormat = NULL;
		}
		if (mt.pUnk != NULL)
		{
			
			/*mt.pUnk->Release();
			mt.pUnk = NULL;*/
		}

		MyDeleteMediaType(&mt);


		 

	
	long Size = 0;
	hr = pC->m_pSampleGrabber->GetCurrentBuffer(&Size, NULL);


	if (FAILED(hr))
                return false;
	if (Size <10000)
return false;


	if (pC->cbBuffer!=Size)

	{
		
		if (pC->cbBuffer!=0)
	{
                    delete[] pBuffer;

	}
		
	pC->cbBuffer=Size;

		pBuffer = new char[pC->cbBuffer];
	}
	if (!pBuffer) 
{
	
	pC->cbBuffer=0;
return false;
	}
	
	
	hr = pC->m_pSampleGrabber->GetCurrentBuffer(&pC->cbBuffer, (long*)pBuffer);
if (FAILED(hr))
                return false;





 StretchDIBits(
  			(HDC)HD, 0, 0, 
			width, height,
			0, 0,
			W, H,
			pBuffer,
			(BITMAPINFO*)&pVih->bmiHeader,
  DIB_RGB_COLORS,
  SRCCOPY
);


		/*SetDIBitsToDevice(
			(HDC)HD, 0, 0, 
			W,
			H,
			0, 0, 
			0,
			H,
			pBuffer,
			(BITMAPINFO*)&pVih->bmiHeader,
			DIB_RGB_COLORS
		);*/

	return true;
}



bool XDMDNative::GrabSampleToDC(int HD)
{

	
	if (pC->m_pSampleGrabber == NULL)
		return false;


	HRESULT hr;

	AM_MEDIA_TYPE mt;
		hr = pC->m_pSampleGrabber->GetConnectedMediaType(&mt);
		

		if ((mt.formattype == FORMAT_VideoInfo) && 
			(mt.cbFormat >= sizeof(VIDEOINFOHEADER)) &&
			(mt.pbFormat != NULL) ) 
		{
				
			pVih = (VIDEOINFOHEADER*)mt.pbFormat;
		
		}
		else 
		{

			
			if (mt.cbFormat != 0)
		{
			CoTaskMemFree((PVOID)mt.pbFormat);
			mt.cbFormat = 0;
			mt.pbFormat = NULL;
		}
		if (mt.pUnk != NULL)
		{
			
			/*mt.pUnk->Release();
			mt.pUnk = NULL;*/
		}

		MyDeleteMediaType(&mt);
			return false; 
		}

		if (mt.cbFormat != 0)
		{
			CoTaskMemFree((PVOID)mt.pbFormat);
			mt.cbFormat = 0;
			mt.pbFormat = NULL;
		}
		if (mt.pUnk != NULL)
		{
			
			/*mt.pUnk->Release();
			mt.pUnk = NULL;*/
		}

		MyDeleteMediaType(&mt);


		 

	
	long Size = 0;
	hr = pC->m_pSampleGrabber->GetCurrentBuffer(&Size, NULL);


	if (FAILED(hr))
                return false;
	if (Size <10000)
return false;


	if (pC->cbBuffer!=Size)

	{
		
		if (pC->cbBuffer!=0)
	{
                    delete[] pBuffer;

	}
		
	pC->cbBuffer=Size;

		pBuffer = new char[pC->cbBuffer];
	}
	if (!pBuffer) 
{
	
	pC->cbBuffer=0;
return false;
	}
	
	
	hr = pC->m_pSampleGrabber->GetCurrentBuffer(&pC->cbBuffer, (long*)pBuffer);
if (FAILED(hr))
                return false;

		SetDIBitsToDevice(
			(HDC)HD, 0, 0, 
			W,
			H,
			0, 0, 
			0,
			H,
			pBuffer,
			(BITMAPINFO*)&pVih->bmiHeader,
			DIB_RGB_COLORS
		);

	return true;
}





bool XDMDNative::GrabSampleToMemory(long buf,long l)
{
	return GrabSampleToMemory(buf,l,false);
}


bool XDMDNative::GrabSampleToMemory(long buf,long l,bool wait)
{

	

	if (pC->m_pSampleGrabber == NULL)
		return false;


	HRESULT hr;
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	

	
	
	
	
	
	
	
	
	
	
	
	

	

	
	long Size = 0;

		
		
		
  
		

try
	{
	if (cntgrabs<3)

{
	cntgrabs++;
	if (cntgrabs==1)
	{
		if (pC->m_pMediaControl != NULL)
	{
	 OAFilterState state;
	pC->m_pMediaControl->GetState(20000, &state);
	}
	}
	
	
	


	
	if (pC->cbBuffer<1000)
	{
	
		
	hr = pC->m_pSampleGrabber->GetCurrentBuffer(&Size, NULL);

	if (FAILED(hr))
                return false;
	if (Size <10000)
return false;


	if (Size >(l-1))
	{
		Size=(l-1);

return false;
	}



	if (pC->cbBuffer!=Size)

	{
		
	
	
 

	
		
	pC->cbBuffer=Size;

		
	}

	if (Size >l)
return false;

	}





	
	
	if (pC->cbBuffer >(l))
	{
		Size=(l-1);

return false;
	}
}
	hr = pC->m_pSampleGrabber->GetCurrentBuffer(&pC->cbBuffer, (long*)buf);
if (FAILED(hr))
                return false;


	}
	catch(HRESULT)
	{
return false;
	}
	


		



	return true;
}


bool XDMDNative::GrabSampleToBuffer(array<Byte> ^buf,long l)
{

	

	if (pC->m_pSampleGrabber == NULL)
		return false;


	HRESULT hr;
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	

	
	
	
	
	
	
	
	
	
	
	
	

	

	
	long Size = 0;

		
		
		
  
		

try
	{
	if (cntgrabs<3)

{
	cntgrabs++;
	if (cntgrabs==1)
	{
		if (pC->m_pMediaControl != NULL)
	{
	 OAFilterState state;
	pC->m_pMediaControl->GetState(20000, &state);
	}
	}
	
	
	


	
	if (pC->cbBuffer<1000)
	{
	
		
	hr = pC->m_pSampleGrabber->GetCurrentBuffer(&Size, NULL);

	if (FAILED(hr))
                return false;
	if (Size <10000)
return false;


	if (Size >(l-1))
	{
		Size=(l-1);

return false;
	}



	if (pC->cbBuffer!=Size)

	{
		
	
	
 

	
		
	pC->cbBuffer=Size;

		
	}

	if (Size >l)
return false;

	}





	
	
	if (pC->cbBuffer >(l))
	{
		Size=(l-1);

return false;
	}
}
	hr = pC->m_pSampleGrabber->GetCurrentBuffer(&pC->cbBuffer, (long*)reinterpret_cast<unsigned char*>(&buf));
	MessageBox(NULL, (LPCTSTR)"a", (LPCTSTR)"", 0);
if (FAILED(hr))
                return false;


	}
	catch(HRESULT)
	{
return false;
	}
	


		



	return true;
}




bool XDMDNative::StopWait()
{

	if (m_State==PLAYER_STATE::PLAYER_STATE_Graph_Stopped1  ) 
return true;

	SetState(PLAYER_STATE::PLAYER_STATE_Graph_Stopped1 );
		HRESULT hr;
	/*	if (pC==NULL )
		{
return false;
		}*/
	if (pC->m_pMediaControl != NULL)
	{
















		hr = pC->m_pMediaControl->Stop();
		
		if (SUCCEEDED(hr))
		{
			SetState(PLAYER_STATE::PLAYER_STATE_Graph_Stopped1);
		LONGLONG now = 0;
		OAFilterState state2;
		pC->m_pMediaControl->GetState(10000, &state2);
		hr = pC->m_pMediaSeeking->SetPositions(&now, AM_SEEKING_AbsolutePositioning, NULL, AM_SEEKING_NoPositioning);
		














		}
			 OAFilterState state;
	pC->m_pMediaControl->GetState(10000, &state);
	
		}

return true;

}


bool XDMDNative::Stop()
{
	
	if (m_State==PLAYER_STATE::PLAYER_STATE_Graph_Stopped1  ) 
return true;

	SetState(PLAYER_STATE::PLAYER_STATE_Graph_Stopped1 );
		HRESULT hr;
	/*	if (pC==NULL )
		{
return false;
		}*/
	if (pC->m_pMediaControl != NULL)
	{
















		hr = pC->m_pMediaControl->Stop();
		
		if (SUCCEEDED(hr))
		{
			SetState(PLAYER_STATE::PLAYER_STATE_Graph_Stopped1);
		LONGLONG now = 0;

		hr = pC->m_pMediaSeeking->SetPositions(&now, AM_SEEKING_AbsolutePositioning, NULL, AM_SEEKING_NoPositioning);
		}














		/*		 OAFilterState state;
	pC->m_pMediaControl->GetState(20000, &state);*/
	}

return true;

}
void XDMDNative::SetVolume(INT volume)
{
    HRESULT hr = 0;

    if (pC->m_pGraphBuilder2 == NULL || pC->m_pBasicAudio == NULL)
        return;

    hr = pC->m_pBasicAudio->put_Volume(volume);
}

void XDMDNative::ConfigureSampleGrabber()
{

    ZeroMemory(&pC->globalmt, sizeof(AM_MEDIA_TYPE));

    pC->globalmt.majortype = MEDIATYPE_Video;
    pC->globalmt.subtype = MEDIASUBTYPE_RGB32;
    pC->globalmt.formattype = FORMAT_VideoInfo;

	/* HDC hdc = GetDC(NULL);
	INT iBitDepth = GetDeviceCaps(hdc, BITSPIXEL);

	ReleaseDC(NULL, hdc);

	switch (iBitDepth)
	{
	case 8:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 16:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 24:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 32:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	default:
		mt.subtype = MEDIASUBTYPE_RGB32;
	} */

    pC->m_pSampleGrabber->SetMediaType(&pC->globalmt);

pC->m_pSampleGrabber->SetBufferSamples (TRUE);
	
	
}


void XDMDNative::ConfigureSampleGrabber24bit()
{

    ZeroMemory(&pC->globalmt, sizeof(AM_MEDIA_TYPE));

    pC->globalmt.majortype = MEDIATYPE_Video;
    pC->globalmt.subtype = MEDIASUBTYPE_RGB24;
    pC->globalmt.formattype = FORMAT_VideoInfo;

	/* HDC hdc = GetDC(NULL);
	INT iBitDepth = GetDeviceCaps(hdc, BITSPIXEL);

	ReleaseDC(NULL, hdc);

	switch (iBitDepth)
	{
	case 8:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 16:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 24:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 32:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	default:
		mt.subtype = MEDIASUBTYPE_RGB32;
	} */

    pC->m_pSampleGrabber->SetMediaType(&pC->globalmt);

pC->m_pSampleGrabber->SetBufferSamples (TRUE);
	
	
}



void XDMDNative::ConfigureSampleGrabber16bit()
{

    ZeroMemory(&pC->globalmt, sizeof(AM_MEDIA_TYPE));

    pC->globalmt.majortype = MEDIATYPE_Video;
    pC->globalmt.subtype = MEDIASUBTYPE_RGB555;
    pC->globalmt.formattype = FORMAT_VideoInfo;

	/* HDC hdc = GetDC(NULL);
	INT iBitDepth = GetDeviceCaps(hdc, BITSPIXEL);

	ReleaseDC(NULL, hdc);

	switch (iBitDepth)
	{
	case 8:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 16:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 24:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	case 32:
		mt.subtype = MEDIASUBTYPE_RGB32;
		break;
	default:
		mt.subtype = MEDIASUBTYPE_RGB32;
	} */

    pC->m_pSampleGrabber->SetMediaType(&pC->globalmt);

pC->m_pSampleGrabber->SetBufferSamples (TRUE);
	
	
}





bool XDMDNative::Open24bit(String^ file, bool WMV, bool m_bAudioOn)
{
	lastaudio=m_bAudioOn;
W=0;
	HRESULT hr;
WMV=true;
	

	if (pC->m_pMediaControl != NULL)
	{
		




















				 OAFilterState state;
	pC->m_pMediaControl->GetState(20000, &state);

	
	}







	ReleaseInterfaces();




	
	
	
	
	
	
 
 

	
	
	
	
	
	
	
	

SetState(PLAYER_STATE::PLAYER_STATE_Graph_Stopped1 );
if (pC->m_pGraphBuilder2==NULL)
{
    pC->m_pGraphBuilder2.CoCreateInstance(CLSID_CaptureGraphBuilder2);
}


if (pC->m_pFilterGraph==NULL)
{
    pC->m_pFilterGraph.CoCreateInstance(CLSID_FilterGraph);
}
    pC->m_pGraphBuilder2->SetFiltergraph(pC->m_pFilterGraph);

	IntPtr ptr = Marshal::StringToHGlobalUni(file);
LPCWSTR str = reinterpret_cast<LPCWSTR>(ptr.ToPointer());




if (grabberFilter ==NULL)
{
   grabberFilter.CoCreateInstance(CLSID_SampleGrabber);


}

if (pC->m_pSampleGrabber == NULL)
{

	grabberFilter->QueryInterface(IID_ISampleGrabber, (PVOID *)&pC->m_pSampleGrabber);


}

ConfigureSampleGrabber24bit();


    hr =  pC->m_pFilterGraph->AddFilter(grabberFilter, L"Sample Grabber");
    if (FAILED(hr))
	{
		
      
        return FALSE;
	}
	

	if (IsLAVInstalled())
	{
		 

		//AddFilterByCLSID(pC->m_pFilterGraph, CLSID_LAVVIDEO, L"LAV Video", &pC->LAVVideo);

		/*if (LAVVideo != NULL)
		{
			LAVVideo.Release();
			LAVVideo = NULL;
		}*/


		pC->LAVVideo.CoCreateInstance(CLSID_LAVVIDEO);

		hr = pC->m_pFilterGraph->AddFilter(pC->LAVVideo, L"LAV Video");

		if (FAILED(hr))
		{


			return FALSE;
		}


			pC->LAVAudio.CoCreateInstance(CLSID_LAVAUDIO);

			hr = pC->m_pFilterGraph->AddFilter(pC->LAVAudio, L"LAV Audio");

			if (FAILED(hr))
			{


				return FALSE;
			}
			
			//AddFilterByCLSID(pC->m_pFilterGraph, CLSID_LAVAUDIO, L"LAV Audio", &pC->LAVAudio);
			/*if (LAVAudio != NULL)
			{
				LAVAudio.Release();
				LAVAudio = NULL;
			}*/
			
		
	}



	hr = pC->m_pFilterGraph->AddSourceFilter( str, L"Video Source", &pC->sourceFilter);
	Marshal::FreeHGlobal(ptr);
    if (FAILED(hr))
	{
		
      
        return FALSE;
	}
	


	if (!IsFFDShowInstalled() && !IsLAVInstalled())
	{
		
	

		if (pC->pColorConvertDMO == NULL)
		{
			hr = pC->pColorConvertDMO.CoCreateInstance(CLSID_DMOWrapperFilter);

			if (SUCCEEDED(hr))
			{

				hr = pC->pColorConvertDMO->QueryInterface(IID_IDMOWrapperFilter, reinterpret_cast<void**>(&pC->pDmoWrapper));

				if (SUCCEEDED(hr))
				{
					hr = pC->pDmoWrapper->Init(__uuidof(CColorConvertDMO), DMOCATEGORY_VIDEO_EFFECT);

					if (SUCCEEDED(hr))
					{
						hr = pC->m_pFilterGraph->AddFilter(pC->pColorConvertDMO, L"Color Convert DMO");
					}
				}
			}
		}
		else
		{
			
			hr = pC->m_pFilterGraph->AddFilter(pC->pColorConvertDMO, L"Color Convert DMO");
		}
	}

	
 
	
	
 
 
	
	CComPtr<IEnumPins> pEnum = NULL;
	CComPtr<IPin> pPin = NULL;

    hr = pC->sourceFilter->EnumPins(&pEnum);
    
    while (S_OK == pEnum->Next(1, &pPin, NULL))
    {
        hr = ConnectFilters(pC->m_pFilterGraph, pPin, grabberFilter);

        pPin.Release();
pPin = NULL;
        if (SUCCEEDED(hr))
        {
            break;
        }
    }
if (pEnum!=NULL)
	pEnum.Release();
 pEnum = NULL;
	
   if (FAILED(hr))
	{
		
      
        return FALSE;
	}
  if (pC->nullRenderer == NULL)  
    pC->nullRenderer.CoCreateInstance(CLSID_NullRenderer);

	hr = pC->m_pFilterGraph->AddFilter(pC->nullRenderer, L"Null Renderer");

    if (FAILED(hr))
	{
		
      
        return FALSE;
	}

	/* if(WMV==TRUE)
		hr = pC->m_pGraphBuilder2->RenderStream(NULL, &MEDIATYPE_Video, sourceFilter, grabberFilter, nullRenderer);
	else
		hr = pC->m_pGraphBuilder2->RenderStream(NULL, NULL, sourceFilter, grabberFilter, nullRenderer); */

	
	hr = ConnectFilters(pC->m_pFilterGraph, grabberFilter, pC->nullRenderer);

    if (FAILED(hr))
	{
		
       
        return FALSE;
	}







    hr = pC->m_pGraphBuilder2->RenderStream(NULL, &MEDIATYPE_Audio, pC->sourceFilter, NULL, NULL);






	



	//hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaEvent,(PVOID *) &pC->m_pMediaEvent);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaControl,(PVOID *) &pC->m_pMediaControl);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaSeeking,(PVOID *) &pC->m_pMediaSeeking);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaPosition,(PVOID *) &pC->m_pMediaPosition);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IBasicAudio,(PVOID *) &pC->m_pBasicAudio);

	//String^ FileName = System::IO::Path::GetFileNameWithoutExtension(file);

	//DisconnectAllPins();
	//RemoveAllFilters();
	//IntPtr ptr2 = Marshal::StringToHGlobalUni("C:\\pinballx\\graph.grf");
	//LPCWSTR str2 = reinterpret_cast<LPCWSTR>(ptr2.ToPointer());
	//SaveGraphFile(pC->m_pFilterGraph, (WCHAR *)str2);


if (m_bAudioOn==true)
{
	SetVolume(VOLUME_FULL);
}
else
{
	SetVolume(VOLUME_SILENCE);
}
	


lastfile =file;
    
	return true;


}	



bool XDMDNative::Open16bit(String^ file, bool WMV, bool m_bAudioOn)
{
	lastaudio=m_bAudioOn;
W=0;
	HRESULT hr;
WMV=true;
	Stop();

	if (pC->m_pMediaControl != NULL)
	{




















if(!IsFFDShowInstalled())
	{
				 OAFilterState state;
	pC->m_pMediaControl->GetState(20000, &state);

}
	}












	
	
	
	
	
	
 
 

	
	
	
	
	
	
	
	ReleaseInterfaces();

SetState(PLAYER_STATE::PLAYER_STATE_Graph_Stopped1 );
if (pC->m_pGraphBuilder2==NULL)
{
    pC->m_pGraphBuilder2.CoCreateInstance(CLSID_CaptureGraphBuilder2);
}

if (pC->m_pFilterGraph==NULL)
{
    pC->m_pFilterGraph.CoCreateInstance(CLSID_FilterGraph);
}
    pC->m_pGraphBuilder2->SetFiltergraph(pC->m_pFilterGraph);

	IntPtr ptr = Marshal::StringToHGlobalUni(file);
LPCWSTR str = reinterpret_cast<LPCWSTR>(ptr.ToPointer());




if(grabberFilter == NULL)
{
	grabberFilter.CoCreateInstance(CLSID_SampleGrabber);


}

if (pC->m_pSampleGrabber == NULL)
{

	grabberFilter->QueryInterface(IID_ISampleGrabber, (PVOID *)&pC->m_pSampleGrabber);


}

ConfigureSampleGrabber16bit();
    hr =  pC->m_pFilterGraph->AddFilter(grabberFilter, L"Sample Grabber");
    if (FAILED(hr))
	{
		
      
        return FALSE;
	}
	





	hr = pC->m_pFilterGraph->AddSourceFilter( str, L"Video Source", &pC->sourceFilter);
	Marshal::FreeHGlobal(ptr);
    if (FAILED(hr))
	{
		
      
        return FALSE;
	}
	

if(!IsFFDShowInstalled())
	{
if (pC->pColorConvertDMO==NULL)
{
	hr = pC->pColorConvertDMO.CoCreateInstance(CLSID_DMOWrapperFilter);

	if (SUCCEEDED(hr))
	{
		
		hr = pC->pColorConvertDMO->QueryInterface(IID_IDMOWrapperFilter, reinterpret_cast<void**>(&pC->pDmoWrapper));

		if (SUCCEEDED(hr))
		{
			hr = pC->pDmoWrapper->Init(__uuidof(CColorConvertDMO), DMOCATEGORY_VIDEO_EFFECT);

			if (SUCCEEDED(hr))
			{
				hr = pC->m_pFilterGraph->AddFilter(pC->pColorConvertDMO, L"Color Convert DMO");
			}
		}
	}
}
else
{
	
	hr = pC->m_pFilterGraph->AddFilter(pC->pColorConvertDMO, L"Color Convert DMO");
}
}
	
 
	
	
 
 
	
	CComPtr<IEnumPins> pEnum = NULL;
	CComPtr<IPin> pPin = NULL;

    hr = pC->sourceFilter->EnumPins(&pEnum);
    
    while (S_OK == pEnum->Next(1, &pPin, NULL))
    {
        hr = ConnectFilters(pC->m_pFilterGraph, pPin, grabberFilter);

        pPin.Release();
pPin = NULL;
        if (SUCCEEDED(hr))
        {
            break;
        }
    }
if (pEnum!=NULL)
	pEnum.Release();
 pEnum = NULL;
	
   if (FAILED(hr))
	{
		
      
        return FALSE;
	}
  if (pC->nullRenderer == NULL)  
    pC->nullRenderer.CoCreateInstance(CLSID_NullRenderer);

	hr = pC->m_pFilterGraph->AddFilter(pC->nullRenderer, L"Null Renderer");

    if (FAILED(hr))
	{
		
      
        return FALSE;
	}

	/* if(WMV==TRUE)
		hr = pC->m_pGraphBuilder2->RenderStream(NULL, &MEDIATYPE_Video, sourceFilter, grabberFilter, nullRenderer);
	else
		hr = pC->m_pGraphBuilder2->RenderStream(NULL, NULL, sourceFilter, grabberFilter, nullRenderer); */

	
	hr = ConnectFilters(pC->m_pFilterGraph, grabberFilter, pC->nullRenderer);

    if (FAILED(hr))
	{
		
       
        return FALSE;
	}

    hr = pC->m_pGraphBuilder2->RenderStream(NULL, &MEDIATYPE_Audio, pC->sourceFilter, NULL, NULL);
 
	
	
 
 
	



	//hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaEvent,(PVOID *) &pC->m_pMediaEvent);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaControl,(PVOID *) &pC->m_pMediaControl);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaSeeking,(PVOID *) &pC->m_pMediaSeeking);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaPosition,(PVOID *) &pC->m_pMediaPosition);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IBasicAudio,(PVOID *) &pC->m_pBasicAudio);



if (m_bAudioOn==true)
{
	SetVolume(VOLUME_FULL);
}
else
{
	SetVolume(VOLUME_SILENCE);
}
	


lastfile =file;
    
	return true;


}	





bool XDMDNative::Open(String^ file, bool WMV, bool m_bAudioOn)
{
	lastaudio=m_bAudioOn;
W=0;
	HRESULT hr;
WMV=true;
	Stop();

	if (pC->m_pMediaControl != NULL)
	{




















if(!IsFFDShowInstalled())
	{
				 OAFilterState state;
	pC->m_pMediaControl->GetState(20000, &state);

	
}
}
	












	
	
	
	
	
	
 
 

	
	
	
	
	
	
	
	ReleaseInterfaces();

SetState(PLAYER_STATE::PLAYER_STATE_Graph_Stopped1 );
if (pC->m_pGraphBuilder2==NULL)
{
    pC->m_pGraphBuilder2.CoCreateInstance(CLSID_CaptureGraphBuilder2);
}

if (pC->m_pFilterGraph==NULL)
{
    pC->m_pFilterGraph.CoCreateInstance(CLSID_FilterGraph);
}
    pC->m_pGraphBuilder2->SetFiltergraph(pC->m_pFilterGraph);

	IntPtr ptr = Marshal::StringToHGlobalUni(file);
LPCWSTR str = reinterpret_cast<LPCWSTR>(ptr.ToPointer());



if (grabberFilter == NULL)
{
	grabberFilter.CoCreateInstance(CLSID_SampleGrabber);


}

if (pC->m_pSampleGrabber == NULL)
{

	grabberFilter->QueryInterface(IID_ISampleGrabber, (PVOID *)&pC->m_pSampleGrabber);


}

ConfigureSampleGrabber();
    hr =  pC->m_pFilterGraph->AddFilter(grabberFilter, L"Sample Grabber");
    if (FAILED(hr))
	{
		
      
        return FALSE;
	}
	





	hr = pC->m_pFilterGraph->AddSourceFilter( str, L"Video Source", &pC->sourceFilter);
	Marshal::FreeHGlobal(ptr);
    if (FAILED(hr))
	{
		
      
        return FALSE;
	}
	

if(!IsFFDShowInstalled())
	{
if (pC->pColorConvertDMO==NULL)
{
	hr = pC->pColorConvertDMO.CoCreateInstance(CLSID_DMOWrapperFilter);

	if (SUCCEEDED(hr))
	{
		
		hr = pC->pColorConvertDMO->QueryInterface(IID_IDMOWrapperFilter, reinterpret_cast<void**>(&pC->pDmoWrapper));

		if (SUCCEEDED(hr))
		{
			hr = pC->pDmoWrapper->Init(__uuidof(CColorConvertDMO), DMOCATEGORY_VIDEO_EFFECT);

			if (SUCCEEDED(hr))
			{
				hr = pC->m_pFilterGraph->AddFilter(pC->pColorConvertDMO, L"Color Convert DMO");
			}
		}
	}
}
else
{
	
	hr = pC->m_pFilterGraph->AddFilter(pC->pColorConvertDMO, L"Color Convert DMO");
}
}
	
 
	
	
 
 
	
	CComPtr<IEnumPins> pEnum = NULL;
	CComPtr<IPin> pPin = NULL;

    hr = pC->sourceFilter->EnumPins(&pEnum);
    
    while (S_OK == pEnum->Next(1, &pPin, NULL))
    {
        hr = ConnectFilters(pC->m_pFilterGraph, pPin, grabberFilter);

        pPin.Release();
pPin = NULL;
        if (SUCCEEDED(hr))
        {
            break;
        }
    }
if (pEnum!=NULL)
	pEnum.Release();
 pEnum = NULL;
	
   if (FAILED(hr))
	{
		
      
        return FALSE;
	}
  if (pC->nullRenderer == NULL)  
    pC->nullRenderer.CoCreateInstance(CLSID_NullRenderer);

	hr = pC->m_pFilterGraph->AddFilter(pC->nullRenderer, L"Null Renderer");

    if (FAILED(hr))
	{
		
      
        return FALSE;
	}

	/* if(WMV==TRUE)
		hr = pC->m_pGraphBuilder2->RenderStream(NULL, &MEDIATYPE_Video, sourceFilter, grabberFilter, nullRenderer);
	else
		hr = pC->m_pGraphBuilder2->RenderStream(NULL, NULL, sourceFilter, grabberFilter, nullRenderer); */

	
	hr = ConnectFilters(pC->m_pFilterGraph, grabberFilter, pC->nullRenderer);

    if (FAILED(hr))
	{
		
       
        return FALSE;
	}

    hr = pC->m_pGraphBuilder2->RenderStream(NULL, &MEDIATYPE_Audio, pC->sourceFilter, NULL, NULL);
 
	
	
 
 
	



	//hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaEvent,(PVOID *) &pC->m_pMediaEvent);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaControl,(PVOID *) &pC->m_pMediaControl);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaSeeking,(PVOID *) &pC->m_pMediaSeeking);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IMediaPosition,(PVOID *) &pC->m_pMediaPosition);
	hr = pC->m_pFilterGraph->QueryInterface(IID_IBasicAudio,(PVOID *) &pC->m_pBasicAudio);



if (m_bAudioOn==true)
{
	SetVolume(VOLUME_FULL);
}
else
{
	SetVolume(VOLUME_SILENCE);
}
	


lastfile =file;
    
	return true;


}	

BOOL XDMDNative::IsLAVInstalled()
{
	if (pC->CheckedLavInstalled==true)
	{
		return pC->ResultCheckLavInstalled;
	}
	pC->CheckedLavInstalled = true;
	pC->ResultCheckLavInstalled = false;
	CComPtr<IBaseFilter> pFFDShowVideo;
	HRESULT hr;
	hr = pFFDShowVideo.CoCreateInstance(CLSID_LAVVIDEO);
	if (pFFDShowVideo != NULL)
	{
		pFFDShowVideo.Release();
		pFFDShowVideo = NULL;
		pC->ResultCheckLavInstalled = SUCCEEDED(hr);
		

	}
	return SUCCEEDED(hr);

}


HRESULT XDMDNative::DisconnectAllPins()
{
	HRESULT hr = S_OK;
	//return hr;
	if (pC->m_pFilterGraph !=NULL)
	{
		CComPtr<IEnumFilters> pIEnumFilters = NULL;
		hr = pC->m_pFilterGraph->EnumFilters(&pIEnumFilters);
		if (SUCCEEDED(hr))
		{
			CComPtr<IBaseFilter> pFilter = NULL;
			//IBaseFilter* pFilter = NULL;
			while (S_OK == pIEnumFilters->Next(1, &pFilter, NULL))
			{
				CComPtr<IEnumPins> pIEnumPins = NULL;
				hr = pFilter->EnumPins(&pIEnumPins);
				if (SUCCEEDED(hr))
				{
					CComPtr < IPin> pIPin = NULL;
					while (S_OK == pIEnumPins->Next(1, &pIPin, NULL))
					{
						CComPtr <IPin> pIPinConnection = NULL;
						if (S_OK == pIPin->ConnectedTo(&pIPinConnection))
						{
							
							hr = pC->m_pFilterGraph->Disconnect(pIPin);
							hr = pC->m_pFilterGraph->Disconnect(pIPinConnection);

							if (pIPinConnection!=NULL)
							{
							pIPinConnection.Release();
							pIPinConnection=NULL;
							}
						}

						if (pIPin!=NULL)
							{
							pIPin.Release();
							pIPin=NULL;
							}
						
					}
				}

				if (pFilter!=NULL)
							{
							pFilter.Release();
							pFilter=NULL;
							}
				
			}
		}
	}
	else
	{
		hr = E_INVALIDARG;
	}

	return hr;
}

BOOL XDMDNative::IsFFDShowInstalled()
{


	if (pC->CheckedFFInstalled==true)
	{
		return pC->ResultCheckFFInstalled;
	}
	pC->CheckedFFInstalled = true;
	pC->ResultCheckFFInstalled = false;

	CComPtr<IBaseFilter> pFFDShowVideo;
	HRESULT hr;
	hr = pFFDShowVideo.CoCreateInstance(CLSID_FFDSHOW);
	if (pFFDShowVideo != NULL)
	{
		pFFDShowVideo.Release();
		pFFDShowVideo = NULL;
		pC->ResultCheckFFInstalled = SUCCEEDED(hr);
		return SUCCEEDED(hr);
	}


	//hr = pFFDShowVideo.CoCreateInstance(CLSID_LAVVIDEO);
	//if (pFFDShowVideo != NULL)
	//{
	//	pFFDShowVideo.Release();
	//	pFFDShowVideo = NULL;
	//	pC->ResultCheckFFInstalled = SUCCEEDED(hr);
	//	return SUCCEEDED(hr);
	//}
	return SUCCEEDED(hr);

}




void XDMDNative::RemoveAllFilters()
{
	CComPtr <IEnumFilters> pEnum;
	//IBaseFilter *pFilter;
	CComPtr<IBaseFilter> pFilter;
	HRESULT hr;

	if (pC->m_pFilterGraph == NULL)
		return;

	hr = pC->m_pFilterGraph->EnumFilters(&pEnum);

	if (SUCCEEDED(hr))
	{
		pEnum->Reset();

		while (pEnum->Next(1, &pFilter, NULL) == S_OK)
		{
			pC->m_pFilterGraph->RemoveFilter(pFilter);
			pEnum->Reset();
			
			pFilter.Release();
			pFilter = NULL;
		}

		pEnum.Release();
		pEnum = NULL;
	}
}

void XDMDNative::ReleaseInterfaces()
{
if (pC==NULL )
		{
return;
		}
HRESULT hr;



OAFilterState state;
                    
					 pC->cbBuffer=0;
		



if (pC->m_pMediaControl != NULL)
{
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	


	

	pC->m_pMediaControl->Stop();







	pC->m_pMediaControl->GetState(20000, &state);
	
	}




if (pC->m_pFilterGraph != NULL)
{
	DisconnectAllPins();
	RemoveAllFilters();
}



















	if (pC->m_pBasicAudio != NULL)
	{

		
		pC->m_pBasicAudio.Release();
		pC->m_pBasicAudio = NULL;
	}
	if (pC->m_pMediaPosition != NULL)
	{
		
		pC->m_pMediaPosition.Release();
		pC->m_pMediaPosition = NULL;
	}


	
	if (pC->m_pMediaSeeking != NULL)
	{
		
		pC->m_pMediaSeeking.Release();
		pC->m_pMediaSeeking = NULL;
	}


		if (pC->m_pMediaControl != NULL)
	{
		

	
	/*	OAFilterState st;
pC->m_pMediaControl->GetState(20000,&st);*/
		pC->m_pMediaControl.Release();
		pC->m_pMediaControl = NULL;
	}



	//if (pC->m_pMediaEvent != NULL)
	//{
	//	pC->m_pMediaEvent->SetNotifyWindow(NULL, 0, NULL);
	//	pC->m_pMediaEvent.Release();
	//	pC->m_pMediaEvent = NULL;
	//}


if (pC->sourceFilter != NULL)
	{
		/*pC->sourceFilter->Stop();
		FILTER_STATE s;
		pC->sourceFilter->GetState(20000, &s);*/
		//pC->sourceFilter.Release();
		//pC->sourceFilter = NULL;
	}











































	
	
	
	
	
	








	
	
	
	
	







	if (pC->LAVVideo != NULL)
	{

		pC->LAVVideo.Release();
		pC->LAVVideo = NULL;
	}

	if (pC->LAVAudio != NULL)
	{

		pC->LAVAudio.Release();
		pC->LAVAudio = NULL;
	}















































































































































if (pC->nullRenderer != NULL)
{
	pC->nullRenderer.Release();
	pC->nullRenderer = NULL;
}
//if (pC->nullRenderer2 != NULL)
//{
//	pC->nullRenderer2.Release();
//	pC->nullRenderer2 = NULL;
//}



//if (pC->grabberFilter != NULL)
//{
//
//	pC->grabberFilter.Release();
//	pC->grabberFilter = NULL;
//}
//
//
//if (pC->m_pSampleGrabber != NULL)
//{
//
//	pC->m_pSampleGrabber.Release();
//	pC->m_pSampleGrabber = NULL;
//}


if (pC->sourceFilter != NULL)
	{
		/*FILTER_STATE s;
		pC->sourceFilter->GetState(20000, &s);
		pC->sourceFilter->*/
		pC->sourceFilter.Release();
		pC->sourceFilter = NULL;
	}





	if (pC->m_pFilterGraph != NULL)
	{
		
		pC->m_pFilterGraph.Release();
		pC->m_pFilterGraph = NULL;
	}




	if (pC->m_pGraphBuilder2 != NULL)
	{

		pC->m_pGraphBuilder2.Release();
		pC->m_pGraphBuilder2 = NULL;
	}



}


void XDMDNative::ReleaseAllInterfacesFinal()
{
	if (pC == NULL)
	{
		return;
	}
	HRESULT hr;



	OAFilterState state;
	OAFilterState state2;

	pC->cbBuffer = 0;




	if (pC->m_pMediaControl != NULL)
	{













		pC->m_pMediaControl->GetState(20000, &state);









		pC->m_pMediaControl->Stop();







		pC->m_pMediaControl->GetState(20000, &state2);

	}























	if (pC->m_pBasicAudio != NULL)
	{


		pC->m_pBasicAudio.Release();
		pC->m_pBasicAudio = NULL;
	}
	if (pC->m_pMediaPosition != NULL)
	{

		pC->m_pMediaPosition.Release();
		pC->m_pMediaPosition = NULL;
	}



	if (pC->m_pMediaSeeking != NULL)
	{

		pC->m_pMediaSeeking.Release();
		pC->m_pMediaSeeking = NULL;
	}


	if (pC->m_pMediaControl != NULL)
	{



		/*	OAFilterState st;
	pC->m_pMediaControl->GetState(20000,&st);*/
		pC->m_pMediaControl.Release();
		pC->m_pMediaControl = NULL;
	}

	//if (pC->m_pMediaEvent != NULL)
	//{
	//	pC->m_pMediaEvent->SetNotifyWindow(NULL, 0, NULL);
	//	pC->m_pMediaEvent.Release();
	//	pC->m_pMediaEvent = NULL;
	//}
	if (pC->m_pFilterGraph != NULL)
	{
		DisconnectAllPins();
		RemoveAllFilters();
	}




	if (pC->sourceFilter != NULL)
	{
		/*pC->sourceFilter->Stop();
		FILTER_STATE s;
		pC->sourceFilter->GetState(20000, &s);*/
		//pC->sourceFilter.Release();
		//pC->sourceFilter = NULL;
	}





































































	if (pC->LAVVideo != NULL)
	{

		pC->LAVVideo.Release();
		pC->LAVVideo = NULL;
	}

	if (pC->LAVAudio != NULL)
	{

		pC->LAVAudio.Release();
		pC->LAVAudio = NULL;
	}















































































































































	if (pC->nullRenderer != NULL)
	{
		pC->nullRenderer.Release();
		pC->nullRenderer = NULL;
	}
	//if (pC->nullRenderer2 != NULL)
	//{
	//	pC->nullRenderer2.Release();
	//	pC->nullRenderer2 = NULL;
	//}






	if (pC->sourceFilter != NULL)
	{
		/*FILTER_STATE s;
		pC->sourceFilter->GetState(20000, &s);
		pC->sourceFilter->*/
		pC->sourceFilter.Release();
		pC->sourceFilter = NULL;
	}


	if (pC->m_pSampleGrabber != NULL)
	{
		//pC->m_pSampleGrabber->SetBufferSamples(FALSE);
		//pC->m_pSampleGrabber->SetMediaType(NULL);
	/*	long Size = 0;
		pC->m_pSampleGrabber->GetCurrentBuffer(&Size, NULL);*/
		pC->m_pSampleGrabber.Release();
		pC->m_pSampleGrabber = NULL;
	}


	//if (pC->grabberFilter != NULL)
	//{
	//	
	//	pC->grabberFilter.Release();
	//	pC->grabberFilter = NULL;
	//}




	//if (pC->m_pGraphBuilder2 != NULL)
	//{

	//	pC->m_pGraphBuilder2->SetFiltergraph(NULL);
	//}

	if (pC->m_pFilterGraph != NULL)
	{

		pC->m_pFilterGraph.Release();
		pC->m_pFilterGraph = NULL;
	}




	if (pC->m_pGraphBuilder2 != NULL)
	{

		pC->m_pGraphBuilder2.Release();
		pC->m_pGraphBuilder2 = NULL;
	}







}


int XDMDNative::VideoWidth()
{
	return W;
}

int XDMDNative::VideoPitch()
{
	return pitch;
}

int XDMDNative::VideoHeight()
{
	return H;
}


LONGLONG XDMDNative::GetDuration()
{
	HRESULT hr = S_OK;
LONGLONG duration;
	if (pC->m_pMediaControl != NULL)
	{
		hr = pC->m_pMediaSeeking->GetDuration(&duration);
		return duration;
	}

	
}
LONGLONG XDMDNative::GetCurrentPosition()
{
	LONGLONG current;
	HRESULT hr = S_OK;

	if (pC->m_pMediaControl != NULL)
	{
		hr = pC->m_pMediaSeeking->GetCurrentPosition(&current);
		return current;
	}

	
}
void XDMDNative::InternalCleanup()
{
	
	ReleaseAllInterfacesFinal();
	

}

STDMETHODIMP XDMDNative::Pause(void)
{
	HRESULT hr;

	if (pC->m_pMediaControl != NULL)
	{
		hr = pC->m_pMediaControl->Pause();
		if (SUCCEEDED(hr))
			SetState(PLAYER_STATE::PLAYER_STATE_Graph_Paused);
	}

	return S_OK;
}


HRESULT XDMDNative::SaveGraphFile(CComPtr<IFilterGraph2> pGraph, WCHAR *wszPath) 
{
    const WCHAR wszStreamName[] = L"ActiveMovieGraph"; 
    HRESULT hr;
    
    IStorage *pStorage = NULL;
    hr = StgCreateDocfile(
        wszPath,
        STGM_CREATE | STGM_TRANSACTED | STGM_READWRITE | STGM_SHARE_EXCLUSIVE,
        0, &pStorage);
    if(FAILED(hr)) 
    {
        return hr;
    }

    IStream *pStream;
    hr = pStorage->CreateStream(
        wszStreamName,
        STGM_WRITE | STGM_CREATE | STGM_SHARE_EXCLUSIVE,
        0, 0, &pStream);
    if (FAILED(hr)) 
    {
        pStorage->Release();    
        return hr;
    }

    IPersistStream *pPersist = NULL;
    pGraph->QueryInterface(IID_IPersistStream, (void**)&pPersist);
    hr = pPersist->Save(pStream, TRUE);
    pStream->Release();
    pPersist->Release();
    if (SUCCEEDED(hr)) 
    {
        hr = pStorage->Commit(STGC_DEFAULT);
    }
    pStorage->Release();
    return hr;
}


STDMETHODIMP XDMDNative::SetRate(double rate)
{
	HRESULT hr = S_OK;

	if (pC->m_pMediaControl != NULL)
	{
		
		
		hr = pC->m_pMediaSeeking->SetRate (rate);
		

	}

	return S_OK;
}

STDMETHODIMP XDMDNative::Seek(LONGLONG position)
{
	HRESULT hr = S_OK;

	if (pC->m_pMediaControl != NULL)
	{
		LONGLONG now = position;
			if (SUCCEEDED(hr))
			SetState(PLAYER_STATE::PLAYER_STATE_Playing);
		hr = pC->m_pMediaSeeking->SetPositions(&now, AM_SEEKING_AbsolutePositioning, NULL, AM_SEEKING_NoPositioning);
		

	}

	return S_OK;
}

void XDMDNative::Converto8bitGray(int w, int h, int depth, array<Byte> ^argbvalues, array<Byte> ^arr)
{
		int b3 = 0;
		int l = 0;
		switch (depth)
		{
			case 4:
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						l = (((w * 4) * y) + (x * 4));
						if (argbvalues[l] > 0)
						{
							b3 = (int)(((int)(argbvalues[l + 1]) + +(int)(argbvalues[l + 2]) + (int)(argbvalues[l + 3])) / 3.0);
						}
						else
						{
							b3 = 0;
						}
						arr[x + (y * w)] = (char)(b3);
					}
				}
				break;
			case 3:
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						l = (((w * 3) * y) + (x * 3));
						b3 = (int)(((int)(argbvalues[l]) + +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 3.0);
						arr[x + (y * w)] = (char)(b3);
					}
				}
				break;
		}
	}


void XDMDNative::Render(System::Drawing::Bitmap ^Source, array<Byte> ^Buffer)
{
		 
BitmapData ^data = Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format8bppIndexed);
	   unsigned char* Buf = reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());	


	  int w=Source->Size.Width;
		  int h=Source->Size.Height;
		  
		  int color=0;
		
		  int xb=0;
		  int yb=0;
		  
	   for (int y = 0; y < (h); y += 6)
			   {
				   
				   xb=0;
				   for (int x = 0; x < (w);  x += 6 )
				   {
					   color=Buffer[xb + (yb * 128)];
								if (color < 16 )
								{
									color=color * 17;
									for (int y2 = (y); y2 < (y+5); y2++)
									{
										for (int x2 = (x); x2 < (x+5); x2++)
										{
											Buf[x2 + (y2 * w)]=color;
										}
									}
								}
						xb++;
					}
					yb++;
				}
	   Source->UnlockBits (data);
}





void XDMDNative::Render(System::Drawing::Bitmap ^Source, array<ARGB> ^Buffer)
{

	BitmapData ^data = Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
	unsigned char* Buf = reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());


	int w = Source->Size.Width;
	int h = Source->Size.Height;
	
	int color = 0;

	int xb = 0;
	int yb = 0;

	for (int y = 0; y < (h); y += 6)
	{

		xb = 0;
		for (int x = 0; x < (w*3); x += 18)
		{
			
				for (int y2 = (y); y2 < (y + 5); y2++)
				{
					for (int x2 = (x); x2 < (x + 15); x2 +=3)
					{
						Buf[(x2 + (y2 * (w * 3)))] = Buffer[xb + (yb * (128))].B;
						Buf[(x2 + (y2 * (w * 3)))+1] = Buffer[xb + (yb * (128 ))].G;
						Buf[(x2 + (y2 * (w * 3))) + 2] = Buffer[xb + (yb * (128 ))].R;
					}
				}

			xb++;
		}
		yb++;
	}
	Source->UnlockBits(data);
}




void XDMDNative::FlipY(int w, int h, array<Byte> ^Source)
{
	Byte b;
	
	unsigned char BufDest[64048];
	pin_ptr<System::Byte> Buf = &Source[0];
	
	int yh = h;
		for (int y = 0; y < (h); y += 1)
		{
			yh -= 1;

				for (int x = 0; x < (w); x += 1)
				{
					
					BufDest[x + (w*(y))] = Buf[x + (w*(yh))];
					



				}



		}
		memcpy(Buf, BufDest, w *h);
}



void XDMDNative::FlipY(int w, int h, array<ARGB> ^Source)
{
	Byte b;
	
	array<ARGB> ^BufDest;

	 BufDest = gcnew array<ARGB>(64048);

	
	
	int yh = h;
	for (int y = 0; y < (h); y += 1)
	{
		yh -= 1;

		for (int x = 0; x < (w); x += 1)
		{
			
			BufDest[x + (w*(y))] = Source[x + (w*(yh))];
			



		}



	}
	for (int y = 0; y < (h); y += 1)
	{
		

		for (int x = 0; x < (w); x += 1)
		{
			
			Source[x + (w*(y))] = BufDest[x + (w*(y))];
			



		}



	}
	


	
}

				   

void XDMDNative::Render(long Source,int w, int h, array<Byte> ^Buffer)
{


		 

	   unsigned char* Buf = reinterpret_cast<unsigned char*>(Source);	

	 /* int m=(Source->Size.Width -1) * (Source->Size.Height -1);
	  int w=Source->Size.Width;
		  int h=Source->Size.Height;*/
		  
		  int color=0;
		
		  int xb=0;
		  int yb=0;
		  
	   for (int y = 0; y < (h); y += 6)
			   {
				   
				   xb=0;
				   for (int x = 0; x < (w);  x += 6 )
				   {
					   for (int y2 = (y); y2 < (y+5); y2++)
						{
						for (int x2 = (x); x2 < (x+5); x2++)
							{
								color=Buffer[xb + (yb * 128)];
								if (color < 16 )
								{
									color=color * 16;
								if (color>255)
			  color=255;

							Buf[x2 + (y2 * w)]=color;
								}

							}
					   }
					   xb++;
					}
				   yb++;
				}
	   
}





void XDMDNative::Clear(System::Drawing::Bitmap ^Source, ARGB color)
{

	BitmapData ^data = Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
	unsigned char* Buf = reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());

	int m = (Source->Size.Width - 1) * (Source->Size.Height - 1);
	int w = Source->Size.Width;
	int h = Source->Size.Height;

	for (int y = 0; y < (h); y += 6)
	{
		for (int x = 0; x < (w); x += 6)
		{
			for (int y2 = (y); y2 < (y + 5); y2++)
			{
				for (int x2 = (x); x2 < (x + 5); x2++)
				{
					Buf[x2 + (y2 * w)] = color.R;
					Buf[(x2 + (y2 * w))+1] = color.G;
					Buf[(x2 + (y2 * w)) + 2] = color.B;
				}
			}
		}
	}
	Source->UnlockBits(data);
}




void XDMDNative::Clear(System::Drawing::Bitmap ^Source, int color)
{

	BitmapData ^data = Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format8bppIndexed);
	unsigned char* Buf = reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());

	int m = (Source->Size.Width - 1) * (Source->Size.Height - 1);
	int w = Source->Size.Width;
	int h = Source->Size.Height;
	color = color * 16;
	if (color>255)
		color = 255;
	if (color<0)
		color = 0;

	for (int y = 0; y < (h); y += 6)
	{
		for (int x = 0; x < (w); x += 6)
		{
			for (int y2 = (y); y2 < (y + 5); y2++)
			{
				for (int x2 = (x); x2 < (x + 5); x2++)
				{
					Buf[x2 + (y2 * w)] = color;
				}
			}
		}
	}
	Source->UnlockBits(data);
}









void XDMDNative::Clear(array<Byte> ^Source, long Size,int color)
{
		 
	/* color=color * 16;
		  if (color>255)
			  color=255;
		  if (color<0)
			  color=0;*/
	pin_ptr<System::Byte> p = &Source[0];
	memset(p,color,Size);







}

void XDMDNative::Clear(array<ARGB> ^Source, long Size, ARGB color)
{

	/* color=color * 16;
	if (color>255)
	color=255;
	if (color<0)
	color=0;*/
	for (int i = 0; i <= (Size);i += 1)
	{
		Source[i] = color;
	}
	
	

	
	
	
	
	

}


void XDMDNative::Clear(long Source, long Size,int color)
{
	 color=color * 16;
		  if (color>255)
			  color=255;
		  if (color<0)
			  color=0;	 
	
unsigned char* buf = reinterpret_cast<unsigned char*>(Source);

for (int x = 0; x <= Size; x++)
					{
						buf[x]=color;
}

}


void XDMDNative::Converto8bitGrayFlipY(int w, int h, int depth, long Source, array<Byte> ^arr)
{
		int b3 = 0;
		int l = 0;
unsigned char* argbvalues = reinterpret_cast<unsigned char*>(Source);

		switch (depth)
		{
			case 4:
				for (int y = (w-1); y > -1; y--)
				{
					for (int x = 0; x < w; x++)
					{
						l = (((w * 4) * y) + (x * 4));
						if (argbvalues[l] > 0)
						{
							b3 = (int)(((int)(argbvalues[l + 1]) + +(int)(argbvalues[l + 2]) + (int)(argbvalues[l + 3])) / 3.0);
						}
						else
						{
							b3 = 0;
						}
						arr[x + (((h-1)-y) * w)] = (char)(b3);
					}
				}
				break;
			case 3:
				for (int y = 0; y < w; y++)
				{
					for (int x = 0; x < w; x++)
					{
						l = (((w * 3) * y) + (x * 3));
						b3 = (int)(((int)(argbvalues[l]) + +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 3.0);
						arr[x + (((h-1)-y) * w)] = (char)(b3);
					}
				}
				break;
		}
	}


void XDMDNative::Converto4bitGrayFlipY(int w, int h, int depth, long Source, array<Byte> ^arr)
{
		int b3 = 0;
		int l = 0;
unsigned char* argbvalues = reinterpret_cast<unsigned char*>(Source);

		switch (depth)
		{
			case 4:
				for (int y = (w-1); y > -1; y--)
				{
					for (int x = 0; x < w; x++)
					{
						l = (((w * 4) * y) + (x * 4));
						if (argbvalues[l] > 0)
						{
							b3 = (int)(((int)(argbvalues[l + 1]) + +(int)(argbvalues[l + 2]) + (int)(argbvalues[l + 3])) / (3 *16));
						}
						else
						{
							b3 = 0;
						}
						arr[x + (((h-1)-y) * w)] = (char)(b3);
					}
				}
				break;
			case 3:
				for (int y = 0; y < h; y++)
				{
					for (int x = 0; x < w; x++)
					{
						l = (((w * 3) * y) + (x * 3));
						b3 = (int)(((int)(argbvalues[l]) + +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / (3 *16));
						arr[x + (((h-1)-y) * w)] = (char)(b3);
					}
				}
				break;
		}
	}




void XDMDNative::Converto4bitGray(int w, int h, int depth, array<Byte> ^argbvalues, array<Byte> ^arr)
{
	int b3 = 0;
	int l = 0;
	switch (depth)
	{
		case 4:
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					l = (((w * 4) * y) + (x * 4));
					if (argbvalues[l] > 0)
					{
						b3 = (int)((((int)(argbvalues[l + 1]) + +(int)(argbvalues[l + 2]) + (int)(argbvalues[l + 3])) / 3.0) / 16);
					}
					else
					{
						b3 = 0;
					}
					arr[x + (y * w)] = (char)(b3);
				}
			}
			break;
		case 3:
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					l = (((w * 3) * y) + (x * 3));
					b3 = (int)((((int)(argbvalues[l]) + +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 3.0) / 16);
					arr[x + (y * w)] = (char)(b3);
				}
			}
			break;
	}

}



void XDMDNative::Converto4bitGrayFlipY(int w, int h, int depth, array<Byte> ^argbvalues, array<Byte> ^arr)
{
	int b3 = 0;
	int l = 0;
	switch (depth)
	{
		case 4:
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					l = (((w * 4) * y) + (x * 4));
					/*if (argbvalues[l] > 0)
					{*/
						
						/*if (b3>15)
							b3=15;*/
					/*}
					else
					{
						b3 = 0;
					}*/
					arr[x + (((h-1)-y) * w)] = (int)(((int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2]) + (int)(argbvalues[l + 3])) / 48) ;
				}
			}
			break;
		case 3:
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					l = (((w * 3) * y) + (x * 3));
					
					arr[x + (((h-1)-y) * w)] = (int)(((int)(argbvalues[l]) + (int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 48);
				}
			}
			break;

		case 2:
			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					l = (((w * 2) * y) + (x * 2));
					
					arr[x + (((h-1)-y) * w)] = (char)(((int)(argbvalues[l]) + (int)(argbvalues[l + 1]) ) /32) ;
				}
			}
			break;
	}

	}



void XDMDNative::Converto4bitGrayFont(int w, int h, int depth, System::Drawing::Bitmap^ Source, array<Byte> ^arr)
{
	   int b3 = 0;
	   int l = 0;
	   BitmapData ^data = Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format32bppPArgb);
	   unsigned char* argbvalues = reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());
	   switch (depth)
	   {
		   case 4:
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 4) * y) + (x * 4));
					  if (argbvalues[l] > 0)
					{
						b3 = (int)((((int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2]) + (int)(argbvalues[l + 3])) / 3.0) / 16);

						b3=15;
						/*if ((int)(argbvalues[l ])==255 && (int)(argbvalues[l + 1])==255 && (int)(argbvalues[l + 2])==255 && (int)(argbvalues[l + 3])==255)
							b3=16;*/
					}
					else
					{
						b3 = 16;
					}
					  
					   arr[x + (y * w)] = (char)(b3);
				   }
			   }
			   break;
		   case 3:
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 3) * y) + (x * 3));
					   b3 = (int)((((int)(argbvalues[l]) + +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 3.0) / 16);
					   arr[x + (y * w)] = (char)(b3);
				   }
			   }
			   break;
	   }
Source->UnlockBits (data);
   }





void XDMDNative::Converto24bitRGB(int w, int h, int depth, System::Drawing::Bitmap ^Source, array<ARGB> ^arr)

{



	int b3 = 0;
	int l = 0;
	BitmapData ^data;
	unsigned char* argbvalues;

	switch (depth)
	{
	case 4:
		data = Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format32bppArgb);
		argbvalues = reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				l = (((w * 4) * y) + (x * 4));
				if (argbvalues[l] > 10)
				{
					arr[x + (y * w)].A = 255;
					arr[x + (y * w)].B = argbvalues[l ];
					arr[x + (y * w)].G = argbvalues[l + 1];
					arr[x + (y * w)].R = argbvalues[l + 2];
				}
				else

				{
					arr[x + (y * w)].A = 0;
					arr[x + (y * w)].R = 0;
					arr[x + (y * w)].G = 0;
					arr[x + (y * w)].B = 0;
				}
			}
		}
		Source->UnlockBits(data);
		break;
	case 3:
		data = Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
		argbvalues = reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				l = (((w * 3) * y) + (x * 3));
				
				
					arr[x + (y * w)].A = 255;
					arr[x + (y * w)].B = argbvalues[l ];
					arr[x + (y * w)].G = argbvalues[l + 1];
					arr[x + (y * w)].R = argbvalues[l + 2];
				
				/*else

				{
					arr[x + (y * w)].A = 0;
					arr[x + (y * w)].R = 0;
					arr[x + (y * w)].G = 0;
					arr[x + (y * w)].B = 0;
				}*/
			}
		}
		Source->UnlockBits(data);
	}

}








void XDMDNative::Converto4bitGray(int w, int h, int depth, System::Drawing::Bitmap^ Source, array<Byte> ^arr)
{
	   int b3 = 0;
	   int l = 0;
	   BitmapData ^data ;
		    unsigned char* argbvalues ;
	   
	   switch (depth)
	   {
		   case 4:
			   data= Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format32bppArgb);
			  argbvalues= reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 4) * y) + (x * 4));
					  if (argbvalues[l] > 10)
					{
						b3 = (int)(((double)((double)((int)argbvalues[l ]) + (int)(argbvalues[l+1]) + (int)(argbvalues[l + 2])) / 48) );
						if (b3>15)
							b3=15;
					}
					else
					{
						b3 = 16;
					}
					   arr[x + (y * w)] = b3;
				   }
			   }
			   Source->UnlockBits (data);
			   break;
		   case 3:
			    data= Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format24bppRgb);
			  argbvalues= reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 3) * y) + (x * 3));
				/*	  if (argbvalues[l] > 10)
					{*/
						b3 = (int)(((double)((double)((int)argbvalues[l ]) + (int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 48) );
						if (b3>15)
							b3=15;
				/*	}
					else
					{
						b3 = 0;
					}*/
					   arr[x + (y * w)] = b3;
				   }
			   }
			   Source->UnlockBits (data);
	   }

   }


void XDMDNative::Converto4bitGray(int w, int h, int depth, long Source, array<Byte> ^arr)
{
	   int b3 = 0;
	   int l = 0;
	   
	   unsigned char* argbvalues = reinterpret_cast<unsigned char*>(Source);
	   switch (depth)
	   {
		   case 4:
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 4) * y) + (x * 4));
					  if (argbvalues[l] > 10)
					{
						b3 = (int)((((double)(int)(argbvalues[l ])  +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 3.0) / 16);
						arr[x + (y * w)] =b3;
					}
					else
					{
						
					}
					   
				   }
			   }
			   break;
		   case 3:
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 3) * y) + (x * 3));
					   b3 = (int)((((int)(argbvalues[l]) + +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 3.0) / 16);
					   arr[x + (y * w)] = (char)(b3);
				   }
			   }
			   break;
	   }

   }

void XDMDNative::ConvertoARGBtoRGB(long size, array<ARGB> ^src, array<RGB24> ^des)

{



	for (int i = 0; i <= size; i++)
	{
		des[i].R = src[i].R;
		des[i].G = src[i].G;
		des[i].B = src[i].B;
	}


}

void XDMDNative::Converto24bitRGB(int w, int h, int depth, long Source, array<ARGB> ^arr)
{
	int b3 = 0;
	int l = 0;
	
	unsigned char* argbvalues = reinterpret_cast<unsigned char*>(Source);
	switch (depth)
	{
	case 4:
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				l = (((w * 4) * y) + (x * 4));
				if (argbvalues[l] > 10)
				{
					arr[x + (y * w)].A = 255;
					arr[x + (y * w)].B = argbvalues[l ];
					arr[x + (y * w)].G = argbvalues[l + 1];
					arr[x + (y * w)].R = argbvalues[l + 2];
				}
				else
				{
					arr[x + (y * w)].A = 0;
					arr[x + (y * w)].R = 0;
					arr[x + (y * w)].G = 0;
					arr[x + (y * w)].B = 0;
				}

			}
		}
		break;
	case 3:
		for (int y = 0; y < h; y++)
		{
			for (int x = 0; x < w; x++)
			{
				{
					arr[x + (y * w)].A = 255;
					arr[x + (y * w)].B = argbvalues[l ];
					arr[x + (y * w)].G = argbvalues[l + 1];
					arr[x + (y * w)].R = argbvalues[l + 2];
				}
			}
		}
		break;
	}

}



void XDMDNative::Converto8bitGray(int w, int h, int depth, System::Drawing::Bitmap ^Source, array<Byte> ^arr)
{
	   int b3 = 0;
	   int l = 0;
	   BitmapData ^data = Source->LockBits(System::Drawing::Rectangle(System::Drawing::Point::Empty, Source->Size), ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format32bppPArgb);
	   unsigned char* argbvalues = reinterpret_cast<unsigned char*>((data->Scan0).ToPointer());
	   switch (depth)
	   {
		   case 4:
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 4) * y) + (x * 4));
					  if (argbvalues[l] > 0)
					{
						b3 = (int)((((int)(argbvalues[l + 1]) + +(int)(argbvalues[l + 2]) + (int)(argbvalues[l + 3])) / 3.0) );
					}
					else
					{
						b3 = 0;
					}
					   arr[x + (y * w)] = (char)(b3);
				   }
			   }
			   break;
		   case 3:
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 3) * y) + (x * 3));
					   b3 = (int)((((int)(argbvalues[l]) + +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 3.0) );
					   arr[x + (y * w)] = (char)(b3);
				   }
			   }
			   break;
	   }
	   Source->UnlockBits (data);
   }

void XDMDNative::Converto8bitGray(int w, int h, int depth,long Source, array<Byte> ^arr)
{
	   int b3 = 0;
	   int l = 0;
	   
	   unsigned char* argbvalues = reinterpret_cast<unsigned char*>(&Source);
	   switch (depth)
	   {
		   case 4:
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 4) * y) + (x * 4));
					  if (argbvalues[l] > 0)
					{
						b3 = (int)((((int)(argbvalues[l + 1]) + +(int)(argbvalues[l + 2]) + (int)(argbvalues[l + 3])) / 3.0) );
					}
					else
					{
						b3 = 0;
					}
					   arr[x + (y * w)] = (char)(b3);
				   }
			   }
			   break;
		   case 3:
			   for (int y = 0; y < h; y++)
			   {
				   for (int x = 0; x < w; x++)
				   {
					   l = (((w * 3) * y) + (x * 3));
					   b3 = (int)((((int)(argbvalues[l]) + +(int)(argbvalues[l + 1]) + (int)(argbvalues[l + 2])) / 3.0) );
					   arr[x + (y * w)] = (char)(b3);
				   }
			   }
			   break;
	   }
}



void XDMDNative::Draw(array<Byte> ^SrcBuffer, array<Byte> ^DisplayBuf, int SrcWidth, int SrcHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY,double SrcRectScale,double DesRectScale)
{
	Draw(SrcBuffer,DisplayBuf,SrcWidth,SrcHeight,128,32,SrcRect,DesRect,Brightness,FlipX,FlipY,SrcRectScale,DesRectScale);

}

void XDMDNative::Draw(array<Byte> ^SrcBuffer, array<Byte> ^DisplayBuf, int SrcWidth, int SrcHeight,int DesWidth, int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY,double SrcRectScale,double DesRectScale)
{

	


	if (SrcRectScale !=1)
		SrcRect=ScaleRectangle(SrcRect,SrcRectScale);
	if (DesRectScale !=1)
		DesRect=ScaleRectangle(DesRect,DesRectScale);

	int newcol=0;
	double x = DesRect->X;
	double y = DesRect->Y;





			if (SrcRect->X<0)
	{
		double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width );
		x+=Math::Abs(SrcRect->X)*(ScaleW2);
		
		SrcRect->X=0;
		
	}

	double ScaleW = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleWF = SrcRect->Width / safe_cast<double>(DesRect->Width);
	double ScaleSW = SrcRect->Width / safe_cast<double>(DesRect->Width);
	if (ScaleSW<1)
	{
		ScaleWF=ScaleSW;
		ScaleSW=1;
		
	}
	else
	{
		ScaleWF=ScaleSW;
		ScaleW=1;
	}

	
		if (SrcRect->Y<0)
	{
		double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);
		
		y+=(Math::Abs(SrcRect->Y)*(ScaleH2));
		
		SrcRect->Y=0;
		
		
	}


	double ScaleH = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleHF = SrcRect->Height / safe_cast<double>(DesRect->Height);

	double ScaleSH = SrcRect->Height / safe_cast<double>(DesRect->Height);
	
	if (ScaleSH<1)
	{
		
		ScaleHF=ScaleSH;
		ScaleSH=1;
	}
	else
	{
		ScaleHF=ScaleSH;
		ScaleH=1;
		
	}

	
	int YI = 0;
	int XI = 0;
	int FinalX = 0;
	int FinalY = 0;
	int maxw =SrcRect->X + (SrcRect->Width - 1);


	if (maxw >= (((SrcRect->X) )+ (((DesWidth ) - x) *ScaleWF))-1)

		maxw= (((SrcRect->X) )+ (((DesWidth )-x) *ScaleWF)-1);
	

	int maxh =SrcRect->Y + (SrcRect->Height  - 1);



		if (maxh >= (((SrcRect->Y) )+ (((DesHeight-( (y)) ) *ScaleHF)))-1)
		{
			maxh= (((SrcRect->Y) )+ (((DesHeight-( (y)) ) *ScaleHF))-1);
			/*	IntPtr ptr = Marshal::StringToHGlobalUni(System::Convert::ToString (maxh/ScaleSH));
LPCWSTR str = reinterpret_cast<LPCWSTR>(ptr.ToPointer());*/
			
			
		}

				if (maxh >= ((SrcHeight )))
		{
			maxh= ((SrcHeight )-1);

			
		}



		if (maxw >= ((SrcWidth )))
		{
			maxw= ((SrcWidth )-1);


		}
		double origx=x;


	for (double srcy = SrcRect->Y; srcy <=maxh ; srcy+=ScaleSH )
	{
		
		
			
			YI = (int)Math::Round(y + ScaleH, 0) -1;
		
			
			for (int iy = (int)y; iy <= YI; iy++)
			{
				
				if (iy>=0)
			{
				FinalY = (int)iy;
							if (FlipY)
							{
								FinalY = (DesHeight-1 )- FinalY;
							}
				x = origx;

				
		
				for (double srcx = SrcRect->X; srcx <= maxw ; srcx+=ScaleSW )
				{
		
				if (SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)] < 16)
							{
		
					XI = (int)Math::Round(x + ScaleW, 0) -1;
					
					
						
					for (int ix =(int)x; ix <= XI; ix++)
					{
		
							if (ix >= 0)
						{
						

		
										DisplayBuf[ix + (FinalY * DesWidth)] = (int)(((double)((SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)] )/ 15.0) * Brightness)  );
										
								}
								
								
					
							
						}

						
						
						
					}
						
					
					
				x += ScaleW;
				}
					
			
				

			}
			
			
			

		}
		y += ScaleH;
		

	}
}



void XDMDNative::DrawVideo(array<Byte> ^SrcBuffer, array<Byte> ^DisplayBuf, int SrcWidth, int SrcHeight,int DesWidth, int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY,double SrcRectScale,double DesRectScale)
{

	


	if (SrcRectScale !=1)
		SrcRect=ScaleRectangle(SrcRect,SrcRectScale);
	if (DesRectScale !=1)
		DesRect=ScaleRectangle(DesRect,DesRectScale);

	int newcol=0;
	double x = DesRect->X;
	double y = DesRect->Y;





			if (SrcRect->X<0)
	{
		double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width );
		x+=Math::Abs(SrcRect->X)*(ScaleW2);
		
		SrcRect->X=0;
		
	}

	double ScaleW = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleWF = SrcRect->Width / safe_cast<double>(DesRect->Width);
	double ScaleSW = SrcRect->Width / safe_cast<double>(DesRect->Width);
	if (ScaleSW<1)
	{
		ScaleWF=ScaleSW;
		ScaleSW=1;
		
	}
	else
	{
		ScaleWF=ScaleSW;
		ScaleW=1;
	}

	
		if (SrcRect->Y<0)
	{
		double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);
		
		y+=(Math::Abs(SrcRect->Y)*(ScaleH2));
		
		SrcRect->Y=0;
		
		
	}


	double ScaleH = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleHF = SrcRect->Height / safe_cast<double>(DesRect->Height);

	double ScaleSH = SrcRect->Height / safe_cast<double>(DesRect->Height);
	
	if (ScaleSH<1)
	{
		
		ScaleHF=ScaleSH;
		ScaleSH=1;
	}
	else
	{
		ScaleHF=ScaleSH;
		ScaleH=1;
		
	}

	
	int YI = 0;
	int XI = 0;
	int FinalX = 0;
	int FinalY = 0;
	int maxw =SrcRect->X + (SrcRect->Width - 1);


	if (maxw >= (((SrcRect->X) )+ (((DesWidth ) - x) *ScaleWF))-1)

		maxw= (((SrcRect->X) )+ (((DesWidth )-x) *ScaleWF)-1);
	

	int maxh =SrcRect->Y + (SrcRect->Height  - 1);



		if (maxh >= (((SrcRect->Y) )+ (((DesHeight-( (y)) ) *ScaleHF)))-1)
		{
			maxh= (((SrcRect->Y) )+ (((DesHeight-( (y)) ) *ScaleHF))-1);

			
		}

				if (maxh >= ((SrcHeight )))
		{
			maxh= ((SrcHeight )-1);

			
		}



		if (maxw >= ((SrcWidth )))
		{
			maxw= ((SrcWidth )-1);


		}
		double origx=x;

		int b=0;

		int l=0;


		
y+=ScaleH*(DesRect->Height)-1;
		maxh=((DesRect->Height-1)*ScaleHF)+maxh;
		/*if (DesRect->Y<0)
			maxh+=Math::Abs (DesRect->Y)*ScaleSH;*/
		 
			
		double srcstart=0;
		for (srcstart = SrcRect->Y; srcstart <= maxh; srcstart+=ScaleSH )
	{
					if (y<=DesHeight-ScaleH )
					break;
					y-=ScaleH;
		}
		 /*if ((int)maxh>SrcHeight-1)
			 maxh=SrcHeight-1;*/
	for (double srcy = (int)srcstart; srcstart <= maxh; srcy+=ScaleSH )
	{
	
		/*if ((int)srcy>SrcHeight-1)
			break;*/
		/*{*/
		
		if (y<0)
			break;
			YI = (int)Math::Round(y + ScaleH, 0) -1;
		
			for (int iy = YI; iy >= (int)y; iy--)
			{
				
			
				
							/*if (FlipY)
							{*/
								
							
				
				
				/*if (iy>DesHeight-1 )
					break;*/
		x = origx;
				for (double srcx = SrcRect->X; srcx <= maxw ; srcx+=ScaleSW )
				{
		
				/*if (SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)] < 16)
							{*/
		
					XI = (int)Math::Round(x + ScaleW, 0) -1;
					
					
						
					for (int ix =(int)x; ix <= XI; ix++)
					{
		
							if (ix >= 0)
						{
						l = ((((int)SrcWidth  * 3) * (int)srcy) + ((int)srcx * 3));
					b= (int)((((int)(SrcBuffer[l]) + (int)(SrcBuffer[l + 1]) + (int)(SrcBuffer[l + 2])) / 3.0) / 16);
					

										DisplayBuf[ix + (iy * DesWidth)] =(int)((((double)((int)(SrcBuffer[l]) + (int)(SrcBuffer[l + 1]) + (int)(SrcBuffer[l + 2])) / 48) /15)*Brightness ) ;
										
						}
								
								
					
							
					}

						
						
						
			
						
					
					
				x += ScaleW;
			
					
			
				

			}
			
			
			

		
		}
		y -= ScaleH;
		

	}
}























































void XDMDNative::Draw(array<ARGB> ^SrcBuffer, array<ARGB> ^DisplayBuf, int SrcWidth, int SrcHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY, double SrcRectScale, double DesRectScale)
{
	Draw(SrcBuffer, DisplayBuf, SrcWidth, SrcHeight, 128, 32, SrcRect, DesRect, Brightness, FlipX, FlipY, SrcRectScale, DesRectScale);

}

void XDMDNative::Draw(array<ARGB> ^SrcBuffer, array<ARGB> ^DisplayBuf, int SrcWidth, int SrcHeight, int DesWidth, int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY, double SrcRectScale, double DesRectScale)
{

	if (Brightness >= 16)
	{
		Brightness=15;
	}


	if (SrcRectScale != 1)
		SrcRect = ScaleRectangle(SrcRect, SrcRectScale);
	if (DesRectScale != 1)
		DesRect = ScaleRectangle(DesRect, DesRectScale);

	int newcol = 0;
	double x = DesRect->X;
	double y = DesRect->Y;





	if (SrcRect->X<0)
	{
		double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width);
		x += Math::Abs(SrcRect->X)*(ScaleW2);

		SrcRect->X = 0;

	}

	double ScaleW = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleWF = SrcRect->Width / safe_cast<double>(DesRect->Width);
	double ScaleSW = SrcRect->Width / safe_cast<double>(DesRect->Width);
	if (ScaleSW<1)
	{
		ScaleWF = ScaleSW;
		ScaleSW = 1;

	}
	else
	{
		ScaleWF = ScaleSW;
		ScaleW = 1;
	}


	if (SrcRect->Y<0)
	{
		double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);

		y += (Math::Abs(SrcRect->Y)*(ScaleH2));

		SrcRect->Y = 0;


	}


	double ScaleH = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleHF = SrcRect->Height / safe_cast<double>(DesRect->Height);

	double ScaleSH = SrcRect->Height / safe_cast<double>(DesRect->Height);

	if (ScaleSH<1)
	{

		ScaleHF = ScaleSH;
		ScaleSH = 1;
	}
	else
	{
		ScaleHF = ScaleSH;
		ScaleH = 1;

	}


	int YI = 0;
	int XI = 0;
	int FinalX = 0;
	int FinalY = 0;
	int maxw = SrcRect->X + (SrcRect->Width - 1);


	if (maxw >= (((SrcRect->X)) + (((DesWidth)-x) *ScaleWF)) - 1)

		maxw = (((SrcRect->X)) + (((DesWidth)-x) *ScaleWF) - 1);
	

	int maxh = SrcRect->Y + (SrcRect->Height - 1);



	if (maxh >= (((SrcRect->Y)) + (((DesHeight - ((y))) *ScaleHF))) - 1)
	{
		maxh = (((SrcRect->Y)) + (((DesHeight - ((y))) *ScaleHF)) - 1);
		/*	IntPtr ptr = Marshal::StringToHGlobalUni(System::Convert::ToString (maxh/ScaleSH));
		LPCWSTR str = reinterpret_cast<LPCWSTR>(ptr.ToPointer());*/
		

	}

	if (maxh >= ((SrcHeight)))
	{
		maxh = ((SrcHeight)-1);


	}



	if (maxw >= ((SrcWidth)))
	{
		maxw = ((SrcWidth)-1);

		
	}
	double origx = x;
	

	for (double srcy = SrcRect->Y; srcy <= maxh; srcy += ScaleSH)
	{



		YI = (int)Math::Round(y + ScaleH, 0) - 1;
		
		
		for (int iy = (int)y; iy <= YI; iy++)
		{

			if (iy >= 0)
			{
				FinalY = (int)iy;
				if (FlipY)
				{
					FinalY = (DesHeight - 1) - FinalY;
				}
				x = origx;



				for (double srcx = SrcRect->X; srcx <= maxw; srcx += ScaleSW)
				{

					

						XI = (int)Math::Round(x + ScaleW, 0) - 1;



						for (int ix = (int)x; ix <= XI; ix++)
						{

							if (ix >= 0)
							{

								if (SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)].A > 10)
								{
									DisplayBuf[ix + (iy * DesWidth)].A = 255;
									DisplayBuf[ix + (iy * DesWidth)].R = (UINT8)(((double)(((double)SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)].R) /255) * Brightness *17));
									DisplayBuf[ix + (iy * DesWidth)].G = (UINT8)(((double)(((double)SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)].G) /255) * Brightness*17));
									DisplayBuf[ix + (iy * DesWidth)].B = (UINT8)(((double)(((double)SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)].B) /255) * Brightness*17));
									
								}
								else

								{

								}
							}




						




					}



					x += ScaleW;
				}




			}




		}
		y += ScaleH;


	}
}





void XDMDNative::Draw(array<ARGB> ^SrcBuffer, array<ARGB> ^DisplayBuf, int SrcWidth, int SrcHeight, int DesWidth, int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY, double SrcRectScale, double DesRectScale, System::Drawing::Color color)
{

	if (Brightness >= 16)
	{
		Brightness = 15;
	}


	if (SrcRectScale != 1)
		SrcRect = ScaleRectangle(SrcRect, SrcRectScale);
	if (DesRectScale != 1)
		DesRect = ScaleRectangle(DesRect, DesRectScale);

	int newcol = 0;
	double x = DesRect->X;
	double y = DesRect->Y;





	if (SrcRect->X<0)
	{
		double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width);
		x += Math::Abs(SrcRect->X)*(ScaleW2);

		SrcRect->X = 0;

	}

	double ScaleW = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleWF = SrcRect->Width / safe_cast<double>(DesRect->Width);
	double ScaleSW = SrcRect->Width / safe_cast<double>(DesRect->Width);
	if (ScaleSW<1)
	{
		ScaleWF = ScaleSW;
		ScaleSW = 1;

	}
	else
	{
		ScaleWF = ScaleSW;
		ScaleW = 1;
	}


	if (SrcRect->Y<0)
	{
		double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);

		y += (Math::Abs(SrcRect->Y)*(ScaleH2));

		SrcRect->Y = 0;


	}


	double ScaleH = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleHF = SrcRect->Height / safe_cast<double>(DesRect->Height);

	double ScaleSH = SrcRect->Height / safe_cast<double>(DesRect->Height);

	if (ScaleSH<1)
	{

		ScaleHF = ScaleSH;
		ScaleSH = 1;
	}
	else
	{
		ScaleHF = ScaleSH;
		ScaleH = 1;

	}


	int YI = 0;
	int XI = 0;
	int FinalX = 0;
	int FinalY = 0;
	int maxw = SrcRect->X + (SrcRect->Width - 1);


	if (maxw >= (((SrcRect->X)) + (((DesWidth)-x) *ScaleWF)) - 1)

		maxw = (((SrcRect->X)) + (((DesWidth)-x) *ScaleWF) - 1);
	

	int maxh = SrcRect->Y + (SrcRect->Height - 1);



	if (maxh >= (((SrcRect->Y)) + (((DesHeight - ((y))) *ScaleHF))) - 1)
	{
		maxh = (((SrcRect->Y)) + (((DesHeight - ((y))) *ScaleHF)) - 1);
		/*	IntPtr ptr = Marshal::StringToHGlobalUni(System::Convert::ToString (maxh/ScaleSH));
		LPCWSTR str = reinterpret_cast<LPCWSTR>(ptr.ToPointer());*/
		

	}

	if (maxh >= ((SrcHeight)))
	{
		maxh = ((SrcHeight)-1);


	}



	if (maxw >= ((SrcWidth)))
	{
		maxw = ((SrcWidth)-1);

		
	}
	double origx = x;
	

	for (double srcy = SrcRect->Y; srcy <= maxh; srcy += ScaleSH)
	{



		YI = (int)Math::Round(y + ScaleH, 0) - 1;
		
		
		for (int iy = (int)y; iy <= YI; iy++)
		{

			if (iy >= 0)
			{
				FinalY = (int)iy;
				if (FlipY)
				{
					FinalY = (DesHeight - 1) - FinalY;
				}
				x = origx;



				for (double srcx = SrcRect->X; srcx <= maxw; srcx += ScaleSW)
				{



					XI = (int)Math::Round(x + ScaleW, 0) - 1;



					for (int ix = (int)x; ix <= XI; ix++)
					{

						if (ix >= 0)
						{

							if (SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)].A > 10)
							{
								DisplayBuf[ix + (iy * DesWidth)].A = 255;
								DisplayBuf[ix + (iy * DesWidth)].R = (UINT8)(((double)color.R / 255) * Brightness * 17);
								DisplayBuf[ix + (iy * DesWidth)].G = (UINT8)(((double)color.G / 255) * Brightness * 17);
								DisplayBuf[ix + (iy * DesWidth)].B = (UINT8)(((double)color.B / 255) * Brightness * 17);
								
							}
							else

							{

							}
						}









					}



					x += ScaleW;
				}




			}




		}
		y += ScaleH;


	}
}




void XDMDNative::DrawVideo(array<Byte> ^SrcBuffer, array<ARGB> ^DisplayBuf, int SrcWidth, int SrcHeight, int DesWidth, int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY, double SrcRectScale, double DesRectScale)
{


	if (Brightness >= 16)
	{
		Brightness = 15;
	}

	if (SrcRectScale != 1)
		SrcRect = ScaleRectangle(SrcRect, SrcRectScale);
	if (DesRectScale != 1)
		DesRect = ScaleRectangle(DesRect, DesRectScale);

	int newcol = 0;
	double x = DesRect->X;
	double y = DesRect->Y;





	if (SrcRect->X<0)
	{
		double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width);
		x += Math::Abs(SrcRect->X)*(ScaleW2);

		SrcRect->X = 0;

	}

	double ScaleW = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleW2 = DesRect->Width / safe_cast<double>(SrcRect->Width);
	double ScaleWF = SrcRect->Width / safe_cast<double>(DesRect->Width);
	double ScaleSW = SrcRect->Width / safe_cast<double>(DesRect->Width);
	if (ScaleSW<1)
	{
		ScaleWF = ScaleSW;
		ScaleSW = 1;

	}
	else
	{
		ScaleWF = ScaleSW;
		ScaleW = 1;
	}


	if (SrcRect->Y<0)
	{
		double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);

		y += (Math::Abs(SrcRect->Y)*(ScaleH2));

		SrcRect->Y = 0;


	}


	double ScaleH = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleH2 = DesRect->Height / safe_cast<double>(SrcRect->Height);
	double ScaleHF = SrcRect->Height / safe_cast<double>(DesRect->Height);

	double ScaleSH = SrcRect->Height / safe_cast<double>(DesRect->Height);

	if (ScaleSH<1)
	{

		ScaleHF = ScaleSH;
		ScaleSH = 1;
	}
	else
	{
		ScaleHF = ScaleSH;
		ScaleH = 1;

	}


	int YI = 0;
	int XI = 0;
	int FinalX = 0;
	int FinalY = 0;
	int maxw = SrcRect->X + (SrcRect->Width - 1);


	if (maxw >= (((SrcRect->X)) + (((DesWidth)-x) *ScaleWF)) - 1)

		maxw = (((SrcRect->X)) + (((DesWidth)-x) *ScaleWF) - 1);
	

	int maxh = SrcRect->Y + (SrcRect->Height - 1);



	if (maxh >= (((SrcRect->Y)) + (((DesHeight - ((y))) *ScaleHF))) - 1)
	{
		maxh = (((SrcRect->Y)) + (((DesHeight - ((y))) *ScaleHF)) - 1);


	}

	if (maxh >= ((SrcHeight)))
	{
		maxh = ((SrcHeight)-1);


	}



	if (maxw >= ((SrcWidth)))
	{
		maxw = ((SrcWidth)-1);

		
	}
	double origx = x;
	
	int b = 0;

	int l = 0;
	

	
	y += ScaleH*(DesRect->Height) - 1;
	maxh = ((DesRect->Height - 1)*ScaleHF) + maxh;
	/*if (DesRect->Y<0)
	maxh+=Math::Abs (DesRect->Y)*ScaleSH;*/
	
	
	double srcstart = 0;
	for (srcstart = SrcRect->Y; srcstart <= maxh; srcstart += ScaleSH)
	{
		if (y <= DesHeight - ScaleH)
			break;
		y -= ScaleH;
	}
	/*if ((int)maxh>SrcHeight-1)
	maxh=SrcHeight-1;*/
	for (double srcy = (int)srcstart; srcstart <= maxh; srcy += ScaleSH)
	{

		/*if ((int)srcy>SrcHeight-1)
		break;*/
		/*{*/
		
		if (y<0)
			break;
		YI = (int)Math::Round(y + ScaleH, 0) - 1;

		for (int iy = YI; iy >= (int)y; iy--)
		{


			
			/*if (FlipY)
			{*/
			
			


			/*if (iy>DesHeight-1 )
			break;*/
			x = origx;
			for (double srcx = SrcRect->X; srcx <= maxw; srcx += ScaleSW)
			{

				/*if (SrcBuffer[(int)srcx + ((int)srcy * SrcWidth)] < 16)
				{*/

				XI = (int)Math::Round(x + ScaleW, 0) - 1;



				for (int ix = (int)x; ix <= XI; ix++)
				{

					if (ix >= 0)
					{
						l = ((((int)SrcWidth * 3) * (int)srcy) + ((int)srcx * 3));
						
						
						DisplayBuf[ix + (iy * DesWidth)].A = 255;
						DisplayBuf[ix + (iy * DesWidth)].B  =(UINT8)((float)((float)SrcBuffer[l] / 255) *(Brightness*17));
						DisplayBuf[ix + (iy * DesWidth)].G = (UINT8)((float)((float)SrcBuffer[l+1] / 255) *(Brightness * 17));
						DisplayBuf[ix + (iy * DesWidth)].R = (UINT8)((float)((float)SrcBuffer[l+2] / 255) *(Brightness * 17));

					}




				}








				x += ScaleW;





			}




			
		}
		y -= ScaleH;


	}
}


























	System::Drawing::Rectangle^ XDMDNative::ScaleRectangle(System::Drawing::Rectangle ^R, double Scale)
{
	System::Drawing::Rectangle ^origr =  System::Drawing::Rectangle(R->X, R->Y, R->Width, R->Height);


	R->Width = (int)(R->Width * Scale);
	R->Height = (int)(R->Height * Scale);
	R->X -= (int)((R->Width / 2.0) - (origr->Width / 2.0));
	R->Y -= (int)((R->Height / 2.0) - (origr->Height / 2.0));
	
	return R;

}

	void XDMDNative::renderDMDFrame(int width, int height, array<ARGB> ^currbuffer)
	{
		
		
		memset(OutputPacketBuffer, 0, 7684);
		int ix;
		int cntrgb = 0;
		for (ix = 0; ix < 4096; ix++) {
			if (cntrgb < 12286) {
				rgbBuffer[cntrgb] = currbuffer[ix].R;
				rgbBuffer[cntrgb + 1] = currbuffer[ix].G;
				rgbBuffer[cntrgb + 2] = currbuffer[ix].B;
			}
			cntrgb += 3;

		}


		OutputPacketBuffer[0] = 0x81;
		OutputPacketBuffer[1] = 0xc3;
		OutputPacketBuffer[2] = 0xe8;
		OutputPacketBuffer[3] = 15; 
		int byteIdx = 4;
		int r3, r4, r5, r6, r7, g3, g4, g5, g6, g7, b3, b4, b5, b6, b7;
		int pixelr, pixelg, pixelb;
		int i, j, v;

		for (j = 0; j <32; j++) {
						for (i = 0; i < 128; i += 8) {
				r3 = 0;
				r4 = 0;
				r5 = 0;
				r6 = 0;
				r7 = 0;

				g3 = 0;
				g4 = 0;
				g5 = 0;
				g6 = 0;
				g7 = 0;

				b3 = 0;
				b4 = 0;
				b5 = 0;
				b6 = 0;
				b7 = 0;

				for (v = 7; v >= 0; v--) {
					pixelr = rgbBuffer[((j * 128) * 3) + ((i + v) * 3)];
					pixelg = rgbBuffer[1 + ((j * 128) * 3) + ((i + v) * 3)];
					pixelb = rgbBuffer[2 + ((j * 128) * 3) + ((i + v) * 3)];

					r3 <<= 1;
					r4 <<= 1;
					r5 <<= 1;
					r6 <<= 1;
					r7 <<= 1;
					g3 <<= 1;
					g4 <<= 1;
					g5 <<= 1;
					g6 <<= 1;
					g7 <<= 1;
					b3 <<= 1;
					b4 <<= 1;
					b5 <<= 1;
					b6 <<= 1;
					b7 <<= 1;

					if (pixelr & 8)
						r3 |= 1;
					if (pixelr & 16)
						r4 |= 1;
					if (pixelr & 32)
						r5 |= 1;
					if (pixelr & 64)
						r6 |= 1;
					if (pixelr & 128)
						r7 |= 1;

					if (pixelg & 8)
						g3 |= 1;
					if (pixelg & 16)
						g4 |= 1;
					if (pixelg & 32)
						g5 |= 1;
					if (pixelg & 64)
						g6 |= 1;
					if (pixelg & 128)
						g7 |= 1;

					if (pixelb & 8)
						b3 |= 1;
					if (pixelb & 16)
						b4 |= 1;
					if (pixelb & 32)
						b5 |= 1;
					if (pixelb & 64)
						b6 |= 1;
					if (pixelb & 128)
						b7 |= 1;
				}
				OutputPacketBuffer[byteIdx + 5120] = r3;
				OutputPacketBuffer[byteIdx + 5632] = r4;
				OutputPacketBuffer[byteIdx + 6144] = r5;
				OutputPacketBuffer[byteIdx + 6656] = r6;
				OutputPacketBuffer[byteIdx + 7168] = r7;

				OutputPacketBuffer[byteIdx + 2560] = g3;
				OutputPacketBuffer[byteIdx + 3072] = g4;
				OutputPacketBuffer[byteIdx + 3584] = g5;
				OutputPacketBuffer[byteIdx + 4096] = g6;
				OutputPacketBuffer[byteIdx + 4608] = g7;

				OutputPacketBuffer[byteIdx + 0] = b3;
				OutputPacketBuffer[byteIdx + 512] = b4;
				OutputPacketBuffer[byteIdx + 1024] = b5;
				OutputPacketBuffer[byteIdx + 1536] = b6;
				OutputPacketBuffer[byteIdx + 2048] = b7;
				byteIdx++;
			}
		}

		
		usb_bulk_write(device, EP_OUT, OutputPacketBuffer, 7684, 5000);
		
		


	}

	void XDMDNative::renderDMDFrame(int width, int height, array<Byte> ^currbuffer)
	{


			int byteIdx=4;
	int bd0,bd1,bd2,bd3;
	int pixel;
	int i,j,v;
	int ret = 0;
	unsigned char frame_buf[2052];

	memset(frame_buf,0,2052);
   frame_buf[0] = 0x81;	
	frame_buf[1] = 0xC3;
	frame_buf[2] = 0xE7;
	frame_buf[3] = 0x0;		

	
	for(j = 0; j < height; j++) {
		
		for(i = 0; i < width; i+=8) {
			bd0 = 0;
			bd1 = 0;
			bd2 = 0;
			bd3 = 0;
			for (v = 7; v >= 0; v--) {
				
				pixel = currbuffer[(j*128) + (i+v)];
				
				bd0 <<= 1;
				bd1 <<= 1;
				bd2 <<= 1;
				bd3 <<= 1;
				
				if(pixel & 1)
					bd0 |= 1;
				if(pixel & 2)
					bd1 |= 1;
				if(pixel & 4)
					bd2 |= 1;
				if(pixel & 8)
					bd3 |= 1;
			}
			frame_buf[byteIdx]      = bd0;
			frame_buf[byteIdx+512]  = bd1;
			frame_buf[byteIdx+1024] = bd2;
			frame_buf[byteIdx+1536] = bd3;
			byteIdx++;
		}
	}

  
  
  
  memcpy(pC->writeBuffer ,frame_buf,2052);
  usb_bulk_write(device, EP_OUT, pC->writeBuffer , 2052, 5000);
  


  /*memcpy(writeBuffer,frame_buf,2048);
	ret = usb_submit_async( asyncWriteContext, writeBuffer, sizeof( writeBuffer ) );
	if( ret < 0 )
    printf("error usb_submit_async:\n%s\n", usb_strerror());

   
   usb_reap_async( asyncWriteContext, 5000 );*/



	}






	
	
	

 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
 
 
 
 

 
 
 
 
 
 
 
 
	
	
	
	
	
	
	
	
	
	
	
	
 
	
	
	
	
 
	


	int XDMDNative::InitPinDMD2()
	{

		int ret = 0;
		struct usb_bus *bus;
		struct usb_device *dev;

		
		usb_init();
		
		usb_find_busses();
		
		usb_find_devices();

		for (bus = usb_get_busses(); bus; bus = bus->next) {
			for (dev = bus->devices; dev; dev = dev->next) {
				
				if (dev->descriptor.idVendor == VID && dev->descriptor.idProduct == PID)
					
					device = usb_open(dev);
				break;
			}
		}

		if (device == NULL) {
			
			
			
			
			return 0;
		}
		
		else
			

			
			if (usb_set_configuration(device, MY_CONFIG) < 0) {
				
				
				
				usb_close(device);
				
				return 0;
			}
		
			else
				

				
				if (usb_claim_interface(device, MY_INTF) < 0) {
					
					
					
					usb_close(device);
					return 0;
				}
		int res = 1;
		

		char string[256];
		ret = usb_get_string_simple(device, dev->descriptor.iManufacturer, string, sizeof(string));
		if (ret > 0) {
			if (!strcmp(string, "PIN2DMD")) {
				
				res = 2;
			}
		}
		
		
		rgbBuffer = (char *)malloc(12288);
		OutputPacketBuffer = (char *)malloc(7684);
		return res;

		return true;
	}

	bool XDMDNative::DisposePinDMD2()
	{
		
		    
    
    
    usb_free_async( &pC->asyncWriteContext );
    
    usb_release_interface( device, MY_INTF );
    
    usb_close( device );
	free(rgbBuffer);
	free(OutputPacketBuffer);
	
	return true;
	}



usb_dev_handle* XDMDNative::open_dev( void )
{
  
  struct usb_bus *bus;
  
  struct usb_device *dev;
  
  
  
  for (bus = usb_get_busses(); bus; bus = bus->next){
    for (dev = bus->devices; dev; dev = dev->next){
      
      if (dev->descriptor.idVendor == VID && dev->descriptor.idProduct == PID)
        return usb_open(dev);
    }
  }
  
  return NULL;
}



int XDMDNative::InitRealDMD()
{
	char filename[MAX_PATH];
	GetModuleFileNameA(NULL, filename, MAX_PATH);
	char *ptr = strrchr(filename, '\\');
#ifdef _WIN64
	strcpy(ptr + 1, "DmdDevice64.dll");
#else
	strcpy(ptr + 1, "DmdDevice.dll");
#endif
	wchar_t wfilename[MAX_PATH];
	mbstowcs(wfilename, filename, strlen(filename) + 1);
	LPWSTR filename_ptr = wfilename;

	hModule = LoadLibraryEx(wfilename, NULL, LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR);

	if (!hModule) {
#ifdef _WIN64
		hModule = LoadLibraryEx(L"DmdDevice64.dll", NULL, LOAD_LIBRARY_SEARCH_DEFAULT_DIRS);
#else
		hModule = LoadLibraryEx(L"DmdDevice.dll", NULL, LOAD_LIBRARY_SEARCH_DEFAULT_DIRS);
#endif
	}

	if (!hModule) {
		return 0;
	}

	DmdDev_Open = (Open_t)GetProcAddress(hModule, "Open");

	DmdDev_Close = (Close_t)GetProcAddress(hModule, "Close");

	DmdDev_Render_16_Shades = (Render_16_Shades_t)GetProcAddress(hModule, "Render_16_Shades");

	DmdDev_Render_RGB24 = (Render_RGB24_t)GetProcAddress(hModule, "Render_RGB24");

	DmdDev_Set_16_Colors_Palette = (Set_16_Colors_Palette_t)GetProcAddress(hModule, "Set_16_Colors_Palette");

	if (!DmdDev_Open || !DmdDev_Close || !DmdDev_Render_16_Shades) {
		MessageBox(NULL, L"DMD device driver functions not found", filename_ptr, MB_ICONERROR);
		return 0;
	}
	else {

		DmdDev_Open();

		if (DmdDev_Render_RGB24)
			return 2;
		else
			return 1;
	}
}

void XDMDNative::SetRealDMDColor(array<RGB24> ^b)
{
	if (DmdDev_Set_16_Colors_Palette) {
		pin_ptr<RGB24> p = &b[0];
		RGB24* palette = p;
		DmdDev_Set_16_Colors_Palette((rgb24*)palette);
	}
}

void XDMDNative::renderDMDFrameDevice(int width, int height, array<RGB24> ^currbuffer)
{
	if (DmdDev_Render_RGB24) {
		pin_ptr<RGB24> p = &currbuffer[0];
		RGB24* buffer = p;
		DmdDev_Render_RGB24(width, height, (rgb24*)buffer);
	}
}

void XDMDNative::renderDMDFrameDevice(int width, int height, array<Byte> ^currbuffer)
{
	pin_ptr<UINT8> p = &currbuffer[0];
	UINT8* tmpbuffer = p;
	DmdDev_Render_16_Shades(width, height, tmpbuffer);
}

bool XDMDNative::DisposeRealDMD()
{
	if (DmdDev_Close)
		DmdDev_Close();
	FreeLibrary(hModule);
	return true;
}


   




	


