

#define VOLUME_FULL		0
#define VOLUME_SILENCE	-10000


	

#pragma once

#using <System.Drawing.dll>

using namespace System;

using namespace System::Drawing;
using namespace System::Drawing::Imaging;


public enum class PLAYER_STATE
    {	PLAYER_STATE_Uninitialized	= 0,
	PLAYER_STATE_Graph_Stopped1	= ( PLAYER_STATE_Uninitialized + 1 ) ,
	PLAYER_STATE_Graph_Stopped2	= ( PLAYER_STATE_Graph_Stopped1 + 1 ) ,
	PLAYER_STATE_Nav_Stopped	= ( PLAYER_STATE_Graph_Stopped2 + 1 ) ,
	PLAYER_STATE_Playing	= ( PLAYER_STATE_Nav_Stopped + 1 ) ,
	PLAYER_STATE_Graph_Paused	= ( PLAYER_STATE_Playing + 1 ) ,
	PLAYER_STATE_Nav_Paused	= ( PLAYER_STATE_Graph_Paused + 1 ) ,
	PLAYER_STATE_Scanning	= ( PLAYER_STATE_Nav_Paused + 1 ) 
    } 	;

public value struct ARGB {
	UINT8 A;
	UINT8 R;
	UINT8 G;
	UINT8 B;
};



public value struct RGB24 {
	UINT8 R;
	UINT8 G;
	UINT8 B;
};

const GUID CLSID_FFDSHOW = { 0x04fe9017, 0xf873, 0x410e, 0x87, 0x1e, 0xab, 0x91, 0x66, 0x1a, 0x4e, 0xf7 };
const GUID CLSID_LAVVIDEO = { 0xEE30215D, 0x164F, 0x4A92, 0xA4, 0xEB, 0x9D, 0x4C, 0x13, 0x39, 0x0F, 0x9F };

const GUID CLSID_LAVAUDIO = { 0xE8E73B6B, 0x4CB3, 0x44A4, 0xBE, 0x99, 0x4F, 0x7B, 0xCB, 0x96, 0xE4, 0x91 };

bool sampleready;

CComPtr<IBaseFilter> grabberFilter;

class C
{
	int value_;

public:
		
	C() 
	{
        asyncReadContext = NULL;
        asyncWriteContext = NULL;
	}
	
		
		void SetValue(int *val) { value_ = *val;}
	const int& GetValue() { return value_; }

	CComPtr<ISampleGrabber> m_pSampleGrabber;
			CComPtr<IMediaControl> m_pMediaControl;
			CComPtr<IBaseFilter> LAVVideo;
			CComPtr<IBaseFilter> LAVAudio;
	CComPtr<IMediaSeeking> m_pMediaSeeking;
			CComPtr<ICaptureGraphBuilder2> m_pGraphBuilder2;
			CComPtr<IFilterGraph2> m_pFilterGraph;
				//CComPtr<IMediaEventEx> m_pMediaEvent;
	CComPtr<IBasicAudio> m_pBasicAudio;
		CComPtr<IMediaPosition> m_pMediaPosition;
				//CComPtr<IBaseFilter> pAudioRender;
						
						CComPtr<IBaseFilter> pColorConvertDMO;
						CComPtr<IDMOWrapperFilter> pDmoWrapper;
						CComPtr<IBaseFilter> nullRenderer;
						//CComPtr<IBaseFilter> nullRenderer2;
						    
							    CComPtr<IBaseFilter> sourceFilter;
						



								bool CheckedLavInstalled;
								bool ResultCheckLavInstalled;
								bool CheckedFFInstalled;
								bool ResultCheckFFInstalled;



									
#define VID 0x0314
#define PID 0xe457



#define MY_CONFIG 1
#define MY_INTF 0


#define EP_IN 0x81
#define EP_OUT 0x01




void* asyncReadContext ;
void* asyncWriteContext ;




#define BUF_SIZE 2052
	
char writeBuffer[ BUF_SIZE ];
char readBuffer[1];

long cbBuffer;

 AM_MEDIA_TYPE globalmt;
};

	public ref class XDMDNative
	{
		
  
  
  
C* pC; 
	public:
~XDMDNative()
{



	InternalCleanup();







	if (pC->m_pFilterGraph != NULL)
	{
		pC->m_pFilterGraph.Release();
		pC->m_pFilterGraph = NULL;
	}



if (pC->pDmoWrapper != NULL)
	{
		
		pC->pDmoWrapper.Release();
		pC->pDmoWrapper = NULL;
	}
if (pC->pColorConvertDMO != NULL)
	{
		pC->pColorConvertDMO.Release();
		pC->pColorConvertDMO = NULL;
	}



if (pC->nullRenderer != NULL)
	{
		pC->nullRenderer.Release();
		pC->nullRenderer = NULL;
	}


//if (pC->nullRenderer2 != NULL)
//{
//	pC->nullRenderer2.Release();
//	pC->nullRenderer2 = NULL;
//}

		if (pC->m_pSampleGrabber != NULL)
	{

		pC->m_pSampleGrabber.Release ();
		pC->m_pSampleGrabber = NULL;
	}


	if (pC->m_pFilterGraph != NULL)
	{
		pC->m_pFilterGraph.Release();
		pC->m_pFilterGraph = NULL;
	}

		if (pC->m_pGraphBuilder2 != NULL)
	{
		pC->m_pGraphBuilder2.Release();
		pC->m_pGraphBuilder2 = NULL;
	}



	//		if (pC->grabberFilter != NULL)
	//{
	//	pC->grabberFilter.Release();
	//	pC->grabberFilter = NULL;
	//}
delete pC;

}
		XDMDNative()


  {
	  
    
	W=0;
	pC = new C();
  }
		

		int FrameCount;
		
char *pBuffer;










































	
 
	
	
	
	
	
	
	
 

  

usb_dev_handle *device ;


usb_dev_handle* open_dev( void );

int port;

void SetPINDMD2Color(array<RGB24> ^b);


void SetRealDMDColor(array<RGB24> ^b);

int InitRealDMD();
bool DisposeRealDMD();

int InitPinDMD2();
bool DisposePinDMD2();

bool UsingDmdDevice;



char *rgbBuffer;
char *OutputPacketBuffer;

void renderDMDFrameDevice(int width, int height, array<Byte> ^currbuffer);
void renderDMDFrameDevice(int width, int height, array<RGB24> ^currbuffer);

void renderDMDFrame(int width, int height, array<Byte> ^currbuffer);
void renderDMDFrame(int width, int height, array<ARGB> ^currbuffer);

void RemoveAllFilters();
HRESULT DisconnectAllPins();
BOOL IsFFDShowInstalled();
BOOL IsLAVInstalled();
	void ConfigureSampleGrabber();
	void ReleaseAllInterfacesFinal();
	void ConfigureSampleGrabber24bit();
	void ConfigureSampleGrabber16bit();

	void Converto8bitGray(int w, int h, int depth, array<Byte> ^argbvalues, array<Byte> ^arr);
	void Converto4bitGray(int w, int h, int depth, array<Byte> ^argbvalues, array<Byte> ^arr);

	void ConvertoARGBtoRGB(long size, array<ARGB> ^src, array<RGB24> ^des);

	
	void Converto4bitGrayFlipY(int w, int h, int depth, array<Byte> ^argbvalues, array<Byte> ^arr);

	void Clear(long Source, long Size,int color);
	void Clear(array<Byte> ^Source, long Size,int color);
	void Clear(array<ARGB> ^Source, long Size, ARGB color);
	void Clear(System::Drawing::Bitmap ^Source, int color);
	void Clear(System::Drawing::Bitmap ^Source, ARGB color);
	void Render(System::Drawing::Bitmap ^Source, array<Byte> ^Buffer);
	void Render(System::Drawing::Bitmap ^Source, array<ARGB> ^Buffer);
	void Render(long Source,int x, int y, array<Byte> ^Buffer);
	void FlipY(int w, int h, array<Byte> ^Buf);
	void FlipY(int w, int h, array<ARGB> ^Buf);
	void Draw(array<Byte> ^SrcBuffer, array<Byte> ^DisplayBuf, int SrcWidth, int SrcHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY,double SrcRectScale,double DesRectScale);
	void Draw(array<Byte> ^SrcBuffer, array<Byte> ^DisplayBuf, int SrcWidth, int SrcHeight,int DesWidth,int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY,double SrcRectScale,double DesRectScale);
	void Draw(array<ARGB> ^SrcBuffer, array<ARGB> ^DisplayBuf, int SrcWidth, int SrcHeight, int DesWidth, int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY, double SrcRectScale, double DesRectScale, System::Drawing::Color color);
	void DrawVideo(array<Byte> ^SrcBuffer, array<Byte> ^DisplayBuf, int SrcWidth, int SrcHeight,int DesWidth,int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY,double SrcRectScale,double DesRectScale);


	void Draw(array<ARGB> ^SrcBuffer, array<ARGB> ^DisplayBuf, int SrcWidth, int SrcHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY, double SrcRectScale, double DesRectScale);
	void Draw(array<ARGB> ^SrcBuffer, array<ARGB> ^DisplayBuf, int SrcWidth, int SrcHeight, int DesWidth, int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY, double SrcRectScale, double DesRectScale);
	void DrawVideo(array<Byte> ^SrcBuffer, array<ARGB> ^DisplayBuf, int SrcWidth, int SrcHeight, int DesWidth, int DesHeight, System::Drawing::Rectangle ^SrcRect, System::Drawing::Rectangle ^DesRect, int Brightness, bool FlipX, bool FlipY, double SrcRectScale, double DesRectScale);


void Converto8bitGrayFlipY(int w, int h, int depth, long Source, array<Byte> ^arr);
void Converto4bitGrayFlipY(int w, int h, int depth, long Source, array<Byte> ^arr);
void Converto24bitRGB(int w, int h, int depth, System::Drawing::Bitmap ^Source, array<ARGB> ^arr);
	
	void Converto4bitGray(int w, int h, int depth, System::Drawing::Bitmap ^Source, array<Byte> ^arr);
	void Converto4bitGrayFont(int w, int h, int depth, System::Drawing::Bitmap ^Source, array<Byte> ^arr);
	void Converto8bitGray(int w, int h, int depth, System::Drawing::Bitmap ^Source, array<Byte> ^arr);
	 System::Drawing::Rectangle^ ScaleRectangle(System::Drawing::Rectangle ^R, double Scale);
void Converto4bitGray(int w, int h, int depth, long Source, array<Byte> ^arr);


void Converto24bitRGB(int w, int h, int depth, long Source, array<ARGB> ^arr);

void Converto8bitGray(int w, int h, int depth, long Source, array<Byte> ^arr);
void _Sleep(int ms);
		bool GrabSampleToMemory(long buf,long l,bool wait);

		bool GrabSampleToBuffer(array<Byte> ^buf,long l);

	bool GrabSampleToMemory(long buf,long l);
	bool Open(System::String^ file, bool WMV, bool m_bAudioOn);
	bool Open24bit(System::String^ file, bool WMV, bool m_bAudioOn);
	bool Open16bit(System::String^ file, bool WMV, bool m_bAudioOn);
	int cntgrabs;
	bool Stop();
	bool StopWait();
	bool Play();
	bool PlayWait();
bool 	m_bFirstPlay;
	int VideoWidth();
	int VideoHeight();
	int VideoPitch();
	STDMETHODIMP Pause(void);
	STDMETHODIMP GrabSample();
	bool GrabSampleToDC(int HD);
	bool GrabSampleToDC(int HD,int width,int height);

	STDMETHODIMP Seek(LONGLONG position);
	STDMETHODIMP SetRate(double rate);
	LONGLONG GetCurrentPosition();
	LONGLONG GetDuration();
	void InternalCleanup();
	PLAYER_STATE State();
	PLAYER_STATE m_State;
	bool UseRGB;
	void SetState(PLAYER_STATE state);










String^ lastfile;
bool lastaudio;

	

private:

	
	LONGLONG LastMem;
		
	void MyDeleteMediaType(AM_MEDIA_TYPE *pmt);
	void SetVolume(INT volume);
	HRESULT SaveGraphFile(CComPtr<IFilterGraph2> pGraph, WCHAR *wszPath);
	void ReleaseInterfaces();
	VIDEOINFOHEADER *pVih; 
		
	int W;
	int H;
	int pitch;
	
		
	};

